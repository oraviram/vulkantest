﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace VulkanTest
{
    unsafe public static class VulkanUtilities
    {
        public static IntPtr[] GetNamePointers<T>(ICollection<string> desiredNames, T[] availablePropertiesArray, string supportedObjectsNameOnFail)
            where T : struct
        {
            if (desiredNames == null || desiredNames.Count == 0)
                return null;

            List<IntPtr> pointers = new List<IntPtr>(desiredNames.Count);
            for (int i = 0; i < availablePropertiesArray.Length; i++)
            {
                IntPtr pointer = Marshal.UnsafeAddrOfPinnedArrayElement(availablePropertiesArray, i);
                if (desiredNames.Contains(Marshal.PtrToStringAnsi(pointer)))
                    pointers.Add(pointer);
            }
            if (pointers.Contains(IntPtr.Zero))
                throw new Exception("Not all " + supportedObjectsNameOnFail + " supported!");

            if (pointers.Count == 0)
                return new IntPtr[] { IntPtr.Zero };

            return pointers.ToArray();
        }

        public static void CallFunctionOnInstance<TFunctionDel>(SharpVulkan.Instance instance, string functionName, Action<TFunctionDel> call, string failMessage = null)
        {
            if (failMessage == null)
                failMessage = "Faild to find function " + functionName + ".";

            IntPtr functionNamePtr = Marshal.StringToHGlobalAnsi(functionName);
            IntPtr functionPointer = instance.GetProcAddress((byte*)functionNamePtr);
            if (functionPointer == IntPtr.Zero)
                throw new Exception(failMessage);
            else
            {
                TFunctionDel function = Marshal.GetDelegateForFunctionPointer<TFunctionDel>(functionPointer);
                call(function);
            }
            Marshal.FreeHGlobal(functionNamePtr);
        }
    }
}
