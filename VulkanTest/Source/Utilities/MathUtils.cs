﻿using System;
using SharpVulkan;

namespace VulkanTest
{
    static class MathUtils
    {
        public static uint Clamp(this uint value, uint min, uint max)
        {
            return Math.Min(Math.Max(value, min), max);
        }

        public static float Clamp(this float value, float min, float max)
        {
            return Math.Min(Math.Max(value, min), max);
        }

        public static Extent2D Clamp(this Extent2D value, Extent2D min, Extent2D max)
        {
            return new Extent2D
            {
                Width = value.Width.Clamp(min.Width, max.Width),
                Height = value.Height.Clamp(min.Height, max.Height),
            };
        }

        /// <summary>
        /// Wraps value so when it gets smaller than min by n it wraps back to max-n again and if it gets bigger than max by n it wraps back to min+n.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        /// <param name="min">The minimum value in the range.</param>
        /// <param name="max">The maximum value in the range.</param>
        /// <returns>Value wrapped between min and max.</returns>
        public static float Wrap(this float value, float min, float max)
        {
            float minToMaxRange = Math.Abs(max - min);
            if (value > max)
            {
                while (value > max)
                    value -= minToMaxRange;
            }
            else
            {
                while (value < min)
                    value += minToMaxRange;
            }
            return value;
        }

        /// <summary>
        /// Wraps value between 0 to length. See <seealso cref="Wrap(float, float, float)"/>.
        /// </summary>
        public static float Wrap(this float value, float length)
        {
            return value.Wrap(0, length);
        }

        public static float PingPong(this float value, float min, float max)
        {
            if (min == max)
                return min;
            
            float minMaxdistance = max - min;
            float error = 0;
            if (value < min)
                error = min - value;
            if (value > max)
                error = value - max;

            value = value.Wrap(min, max);
            if (Math.Ceiling(error / minMaxdistance) % 2 != 0)
                value = max - (value - min);

            return value;
        }

        public static float PingPong(this float value, float length)
        {
            return value.PingPong(0, length);
        }
    }
}