﻿using System;
using System.Numerics;
using System.Diagnostics;

// OBJECTIVE (again?): Try to keep the amount of those to a minimum.
using Version = SharpVulkan.Version;
using Fence = SharpVulkan.Fence;
using Semaphore = SharpVulkan.Semaphore;
using CommandBufferUsageFlags = SharpVulkan.CommandBufferUsageFlags;

namespace VulkanTest
{
    // OBJECTIVE: Make a general vulkan application with no vulkan calls and references (except for special cases, with some removed later) from this file.

    unsafe static class Program
    {
#if DEBUG
        const bool ENABLE_DEBUG_REPORT = true;
#else
        const bool ENABLE_DEBUG_REPORT = false;
#endif

        static Device device;
        static Presenter presenter;
        static CommandPool graphicsCommandPool;
        static CommandBuffer commandBuffer;

        static Fence imageAquiredFence;
        static Semaphore readyToPresentSemaphore;

        const float CLEAR_COLOR_RED_BLUE_ROTATION_SPEED = 2; // In radians per second.
        static float clearColorRedBlueRotationAngle; // In radians. (The actual angle will be a ping pong of this between 0 and pi/2.)
        static readonly Vector4 initialClearColor = new Vector4(1, 0, 0, 1);
        static Vector4 clearColor = new Vector4(1, 0, 0, 1);

        static Stopwatch clock = new Stopwatch();
        static float timeSinceLastFrame; // In seconds.
        static float lastFrameTime; // In seconds.
        
        static void Main()
        {
            ApplicationInfo applicationInfo = new ApplicationInfo
            {
                Name = "Vulkan Test",
                Version = new Version(1, 0, 0),
                EngineName = "Vulkan Test Engine",
                EngineVersion = new Version(1, 0, 0),
            };
            Window window = new Window("Vulkan Test", 1920, 1080, SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN | SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE);
            VulkanInstance instance = new VulkanInstance(ENABLE_DEBUG_REPORT, ref applicationInfo);
            device = instance.CreateDevice();
            presenter = device.CreatePresenter(window);
            graphicsCommandPool = device.CreateCommandPool(device.GraphicsQueueFamilyIndex, SharpVulkan.CommandPoolCreateFlags.Transient | SharpVulkan.CommandPoolCreateFlags.ResetCommandBuffer);
            commandBuffer = graphicsCommandPool.AllocateCommandBuffer();

            imageAquiredFence = device.CreateFence(false);
            readyToPresentSemaphore = device.CreateSemaphore();

            clock.Start();
            window.OnUpdate += UpdateApplication;
            window.OnUpdate += DrawFrame;
            window.EnterLoop();
            clock.Stop();
            device.QueueWaitIdle(device.GraphicsQueueFamilyIndex);

            device.DestroySemaphore(readyToPresentSemaphore);
            device.DestroyFence(imageAquiredFence);

            graphicsCommandPool.Dispose();
            presenter.Dispose();
            window.Dispose();
            device.Dispose();
            instance.Dispose();
            window.Dispose();

            Console.ReadKey(true);
        }

        static void UpdateApplication()
        {
            timeSinceLastFrame = (float)clock.Elapsed.TotalSeconds - lastFrameTime;

            Vector2 rb = new Vector2(initialClearColor.X, initialClearColor.Z);
            clearColorRedBlueRotationAngle += CLEAR_COLOR_RED_BLUE_ROTATION_SPEED * timeSinceLastFrame;
            rb = Vector2.Transform(rb, Matrix3x2.CreateRotation(clearColorRedBlueRotationAngle.PingPong((float)Math.PI / 2)));

            clearColor = new Vector4(
                rb.X, 0, rb.Y, 1);

            lastFrameTime = (float)clock.Elapsed.TotalSeconds;
        }

        static void DrawFrame()
        {
            uint imageIndex = presenter.AcquireNextImage(imageAquiredFence);
            device.WaitForFence(imageAquiredFence);
            device.ResetFence(imageAquiredFence);
            device.QueueWaitIdle(device.GraphicsQueueFamilyIndex);

            commandBuffer.StartRecording(CommandBufferUsageFlags.OneTimeSubmit);
            commandBuffer.BeginRenderPass(presenter.RenderPass, presenter.GetFrameBuffer((int)imageIndex), clearColor);

            commandBuffer.EndRenderPass();
            commandBuffer.StopRecording();

            graphicsCommandPool.SubmitToQueue(commandBuffer, readyToPresentSemaphore, Fence.Null);
            presenter.Present(imageIndex, readyToPresentSemaphore);
        }
    }
}