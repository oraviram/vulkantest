﻿using System;
using System.Numerics;

using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class CommandBuffer
    {
        private Vk.CommandBuffer buffer;

        public CommandBuffer(Vk.CommandBuffer buffer)
        {
            this.buffer = buffer;
        }

        public void StartRecording(Vk.CommandBufferUsageFlags flags)
        {
            Vk.CommandBufferBeginInfo beginInfo = new Vk.CommandBufferBeginInfo
            {
                StructureType = Vk.StructureType.CommandBufferBeginInfo,
                Flags = flags,
                InheritanceInfo = IntPtr.Zero,
            };
            buffer.Begin(ref beginInfo);
        }

        public void BeginRenderPass(RenderPass renderPass, FrameBuffer frameBuffer, Vector4 clearColor)
        {
            // TODO: Do validations before using Float32 and add a depth stencil buffer.
            Vk.ClearValue* clearValues = stackalloc Vk.ClearValue[1];
            clearValues[0].Color.Float32.Value0 = clearColor.X;
            clearValues[0].Color.Float32.Value1 = clearColor.Y;
            clearValues[0].Color.Float32.Value2 = clearColor.Z;
            clearValues[0].Color.Float32.Value3 = clearColor.W;
            
            renderPass.Begin(frameBuffer, buffer, clearValues, 1);
        }

        public void EndRenderPass()
        {
            buffer.EndRenderPass();
        }

        public void StopRecording()
        {
            buffer.End();
        }
    }
}