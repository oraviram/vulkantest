﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Vk = SharpVulkan;

namespace VulkanTest
{
    struct Devices
    {
        public Vk.PhysicalDevice physicalDevice;
        public Vk.Device device;
    }

    /// <summary>
    /// Represents a physical and logical device.
    /// </summary>
    unsafe class Device : IDisposable
    {
        private Devices devices;

        private const uint QUEUE_FAMILY_INDEX_INVALID = uint.MaxValue;
        public uint GraphicsQueueFamilyIndex { get; private set; }
        public uint TransferQueueFamilyIndex { get; private set; }
        private Dictionary<uint, Vk.Queue> queues;

        private Vk.Instance instance;

        /// <summary>
        /// Use <seealso cref="VulkanInstance.CreateDevice"/>.
        /// </summary>
        public Device(Vk.Instance instance, bool enableDebugReport)
        {
            this.instance = instance;

            List<string> deviceExtensionNames = new List<string> { "VK_KHR_swapchain" };
            List<string> deviceLayerNames = new List<string>();
            if (enableDebugReport)
                deviceLayerNames.Add("VK_LAYER_LUNARG_standard_validation");

            ChoosePhysicalDevice(instance.PhysicalDevices);

            uint queueFamilyCount = GraphicsQueueFamilyIndex == TransferQueueFamilyIndex ? (uint)1 : 2;
            uint* queueFamilyIndices = stackalloc uint[(int)queueFamilyCount];
            queueFamilyIndices[0] = GraphicsQueueFamilyIndex;
            if (GraphicsQueueFamilyIndex != TransferQueueFamilyIndex)
                queueFamilyIndices[1] = TransferQueueFamilyIndex;

            float* queuePriorities = stackalloc float[1];
            queuePriorities[0] = 1;

            Vk.DeviceQueueCreateInfo* queueCreateInfos = stackalloc Vk.DeviceQueueCreateInfo[(int)queueFamilyCount];
            for (int i = 0; i < queueFamilyCount; i++)
            {
                queueCreateInfos[i] = new Vk.DeviceQueueCreateInfo
                {
                    StructureType = Vk.StructureType.DeviceQueueCreateInfo,
                    QueueCount = 1, // TODO: Allow multiple queues in a single family.
                    QueueFamilyIndex = queueFamilyIndices[i],
                    QueuePriorities = (IntPtr)queuePriorities,
                };
            }

            IntPtr[] extensionNamesPointers = VulkanUtilities.GetNamePointers(deviceExtensionNames, devices.physicalDevice.GetDeviceExtensionProperties(), "device extensions");
            IntPtr[] layerNamesPointers = VulkanUtilities.GetNamePointers(deviceLayerNames, devices.physicalDevice.DeviceLayerProperties, "device layers");
            Vk.DeviceCreateInfo createInfo = new Vk.DeviceCreateInfo
            {
                StructureType = Vk.StructureType.DeviceCreateInfo,
                QueueCreateInfoCount = queueFamilyCount,
                QueueCreateInfos = (IntPtr)queueCreateInfos,
                EnabledExtensionCount = extensionNamesPointers == null ? 0 : (uint)extensionNamesPointers.Length,
                EnabledExtensionNames = extensionNamesPointers == null ? IntPtr.Zero : Marshal.UnsafeAddrOfPinnedArrayElement(extensionNamesPointers, 0),
                EnabledLayerCount = layerNamesPointers == null ? 0 : (uint)layerNamesPointers.Length,
                EnabledLayerNames = layerNamesPointers == null ? IntPtr.Zero : Marshal.UnsafeAddrOfPinnedArrayElement(layerNamesPointers, 0),
                EnabledFeatures = IntPtr.Zero, // TODO: Add this.
            };
            devices.device = devices.physicalDevice.CreateDevice(ref createInfo);

            queues = new Dictionary<uint, Vk.Queue>((int)queueFamilyCount)
            {
                [GraphicsQueueFamilyIndex] = devices.device.GetQueue(GraphicsQueueFamilyIndex, 0),
                [TransferQueueFamilyIndex] = devices.device.GetQueue(TransferQueueFamilyIndex, 0),
            };
        }

        private void ChoosePhysicalDevice(Vk.PhysicalDevice[] availableDevices)
        {
            int bestDeviceScore = 0;
            foreach (Vk.PhysicalDevice device in availableDevices)
            {
                int score = RatePhysicalDevice(device);
                if (score > bestDeviceScore)
                    devices.physicalDevice = device;
            }
            if (devices.physicalDevice == Vk.PhysicalDevice.Null)
                throw new Exception("No fitting physical device found!");
        }

        private int RatePhysicalDevice(Vk.PhysicalDevice device)
        {
            int score = 1;

            Vk.QueueFamilyProperties[] queueFamilyPropertiesArray = device.QueueFamilyProperties;
            for (uint i = 0; i < queueFamilyPropertiesArray.Length; i++)
            {
                Vk.QueueFamilyProperties queueFamilyProperties = queueFamilyPropertiesArray[i];
                if (queueFamilyProperties.QueueFlags.HasFlag(Vk.QueueFlags.Graphics))
                    GraphicsQueueFamilyIndex = i;

                if (queueFamilyProperties.QueueFlags.HasFlag(Vk.QueueFlags.Transfer) && !queueFamilyProperties.QueueFlags.HasFlag(Vk.QueueFlags.Graphics))
                    TransferQueueFamilyIndex = i;
            }
            if (GraphicsQueueFamilyIndex == QUEUE_FAMILY_INDEX_INVALID)
                return 0;

            if (TransferQueueFamilyIndex == QUEUE_FAMILY_INDEX_INVALID)
                TransferQueueFamilyIndex = GraphicsQueueFamilyIndex;
            else
                score += 5;

            device.GetProperties(out Vk.PhysicalDeviceProperties properties);
            if (properties.DeviceType == Vk.PhysicalDeviceType.DiscreteGpu)
                score += 10;

            return score;
        }

        /// <summary>
        /// Creates a presenter to window.
        /// </summary>
        public Presenter CreatePresenter(Window window)
        {
            return new Presenter(window, instance, devices, GraphicsQueueFamilyIndex);
        }

        // TODO: Add a method to create a render pass when the problems with the attachments are fixed and you can generalize things.

        public CommandPool CreateCommandPool(uint queueFamilyIndex, Vk.CommandPoolCreateFlags flags)
        {
            return new CommandPool(devices.device, queueFamilyIndex, flags);
        }

        public Vk.Fence CreateFence(bool signaledOnCreation)
        {
            Vk.FenceCreateInfo createInfo = new Vk.FenceCreateInfo
            {
                StructureType = Vk.StructureType.FenceCreateInfo,
                Flags = signaledOnCreation ? Vk.FenceCreateFlags.Signaled : Vk.FenceCreateFlags.None,
            };
            return devices.device.CreateFence(ref createInfo);
        }
        public void DestroyFence(Vk.Fence fence) => devices.device.DestroyFence(fence);

        public Vk.Semaphore CreateSemaphore()
        {
            Vk.SemaphoreCreateInfo createInfo = new Vk.SemaphoreCreateInfo
            {
                StructureType = Vk.StructureType.SemaphoreCreateInfo,
            };
            return devices.device.CreateSemaphore(ref createInfo);
        }
        public void DestroySemaphore(Vk.Semaphore semaphore) => devices.device.DestroySemaphore(semaphore);

        // TODO: Add WaitForFences (or modify this).
        public void WaitForFence(Vk.Fence fence)
        {
            devices.device.WaitForFences(1, &fence, true, ulong.MaxValue);
        }

        // TODO: Add ResetFences (or modify this).
        public void ResetFence(Vk.Fence fence)
        {
            devices.device.ResetFences(1, &fence);
        }

        public void QueueWaitIdle(uint queueFamilyIndex)
        {
            queues[queueFamilyIndex].WaitIdle();
        }

        public void Dispose()
        {
            devices.device.Destroy();
        }
    }
}