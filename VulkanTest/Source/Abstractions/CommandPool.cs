﻿using System;
using System.Collections.Generic;

using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class CommandPool : IDisposable
    {
        private Vk.CommandPool pool;

        private Vk.Device device;
        private Vk.Queue queue; // TODO: The normal thing with multiple queues allowance and stuff...

        private Dictionary<CommandBuffer, Vk.CommandBuffer> commandBuffers = new Dictionary<CommandBuffer, Vk.CommandBuffer>();

        public CommandPool(Vk.Device device, uint queueFamilyIndex, Vk.CommandPoolCreateFlags flags)
        {
            this.device = device;
            queue = device.GetQueue(queueFamilyIndex, 0);

            Vk.CommandPoolCreateInfo createInfo = new Vk.CommandPoolCreateInfo
            {
                StructureType = Vk.StructureType.CommandPoolCreateInfo,
                Flags = flags,
                QueueFamilyIndex = queueFamilyIndex,
            };
            pool = device.CreateCommandPool(ref createInfo);
        }

        public CommandBuffer[] AllocateCommandBuffers(uint count)
        {
            Vk.CommandBufferAllocateInfo allocateInfo = new Vk.CommandBufferAllocateInfo
            {
                StructureType = Vk.StructureType.CommandBufferAllocateInfo,
                CommandBufferCount = count,
                CommandPool = pool,
                Level = Vk.CommandBufferLevel.Primary, // TODO: Allow for secondary buffers.
            };
            Vk.CommandBuffer* nativeCommandBuffers = stackalloc Vk.CommandBuffer[(int)count];
            device.AllocateCommandBuffers(ref allocateInfo, nativeCommandBuffers);

            CommandBuffer[] commandBuffers = new CommandBuffer[count];
            for (int i = 0; i < count; i++)
            {
                commandBuffers[i] = new CommandBuffer(nativeCommandBuffers[i]);
                this.commandBuffers.Add(commandBuffers[i], nativeCommandBuffers[i]);
            }
            return commandBuffers;
        }

        public CommandBuffer AllocateCommandBuffer()
        {
            Vk.CommandBufferAllocateInfo allocateInfo = new Vk.CommandBufferAllocateInfo
            {
                StructureType = Vk.StructureType.CommandBufferAllocateInfo,
                CommandBufferCount = 1,
                CommandPool = pool,
                Level = Vk.CommandBufferLevel.Primary, // TODO: Allow for secondary buffers.
            };
            Vk.CommandBuffer nativeCommandBuffer;
            device.AllocateCommandBuffers(ref allocateInfo, &nativeCommandBuffer);

            CommandBuffer commandBuffer = new CommandBuffer(nativeCommandBuffer);
            commandBuffers.Add(commandBuffer, nativeCommandBuffer);
            return commandBuffer;
        }

        public void FreeCommandBuffers(CommandBuffer[] buffers)
        {
            Vk.CommandBuffer* nativeBuffers = stackalloc Vk.CommandBuffer[buffers.Length];
            for (int i = 0; i < buffers.Length; i++)
                nativeBuffers[i] = commandBuffers[buffers[i]];

            device.FreeCommandBuffers(pool, (uint)buffers.Length, nativeBuffers);
        }

        public void FreeCommandBuffer(CommandBuffer buffer)
        {
            Vk.CommandBuffer* nativeBufferPointer = stackalloc Vk.CommandBuffer[1];
            nativeBufferPointer[0] = commandBuffers[buffer];
            device.FreeCommandBuffers(pool, 1, nativeBufferPointer);
        }

        // TODO: Allow for multiple signal semaphores, allow for multiple submits and multiple command buffers for each submit, and allow for wait semaphores.
        public void SubmitToQueue(CommandBuffer commandBuffer, Vk.Semaphore signalSemaphore, Vk.Fence signalFence)
        {
            Vk.CommandBuffer* commandBufferPointer = stackalloc Vk.CommandBuffer[1];
            commandBufferPointer[0] = commandBuffers[commandBuffer];

            Vk.SubmitInfo submitInfo = new Vk.SubmitInfo
            {
                StructureType = Vk.StructureType.SubmitInfo,
                CommandBufferCount = 1,
                CommandBuffers = (IntPtr)commandBufferPointer,
                WaitSemaphoreCount = 0,
                WaitSemaphores = IntPtr.Zero,
                WaitDstStageMask = IntPtr.Zero,
                SignalSemaphoreCount = 1,
                SignalSemaphores = new IntPtr(&signalSemaphore),
            };
            queue.Submit(1, &submitInfo, signalFence);
        }

        public void Dispose()
        {
            device.DestroyCommandPool(pool);
        }
    }
}