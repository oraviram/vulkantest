﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class RenderPass : IDisposable
    {
        private Vk.RenderPass renderPass;
        private Vk.Device device;

        private Dictionary<FrameBuffer, Vk.Framebuffer> frameBuffers = new Dictionary<FrameBuffer, Vk.Framebuffer>();

        /// <summary>
        /// A terrible thing: The only allowed attachment is color attachment.
        /// </summary>
        public RenderPass(Vk.Device device, Vk.Format format, uint colorAttachment)
        {
            this.device = device;

            Vk.AttachmentReference colorAttachmentReference = new Vk.AttachmentReference
            {
                Attachment = colorAttachment,
                Layout = Vk.ImageLayout.ColorAttachmentOptimal,
            };

            Vk.AttachmentDescription* attachmentDescriptions = stackalloc Vk.AttachmentDescription[2];
            attachmentDescriptions[0] = new Vk.AttachmentDescription
            {
                InitialLayout = Vk.ImageLayout.Undefined,
                FinalLayout = Vk.ImageLayout.PresentSource,
                Format = format,
                Samples = Vk.SampleCountFlags.Sample1,
                LoadOperation = Vk.AttachmentLoadOperation.Clear,
                StoreOperation = Vk.AttachmentStoreOperation.Store,
                StencilLoadOperation = Vk.AttachmentLoadOperation.DontCare,
                StencilStoreOperation = Vk.AttachmentStoreOperation.DontCare,
            };

            Vk.SubpassDescription subpass = new Vk.SubpassDescription
            {
                ColorAttachmentCount = 1, // TODO: Allow to use more color attachments.
                ColorAttachments = new IntPtr(&colorAttachmentReference),

                // TODO: Allow for those attachments.
                DepthStencilAttachment = IntPtr.Zero,
                PreserveAttachmentCount = 0,
                PreserveAttachments = IntPtr.Zero,
                ResolveAttachments = IntPtr.Zero,
                InputAttachmentCount = 0,
                InputAttachments = IntPtr.Zero,
                PipelineBindPoint = Vk.PipelineBindPoint.Graphics, // TODO: Allow for compute.
            };

            Vk.RenderPassCreateInfo createInfo = new Vk.RenderPassCreateInfo
            {
                StructureType = Vk.StructureType.RenderPassCreateInfo,
                AttachmentCount = 1, // TODO: Allow more or less
                Attachments = (IntPtr)attachmentDescriptions,
                SubpassCount = 1, // TODO: Allow for multiple subpasses.
                Subpasses = new IntPtr(&subpass),
                DependencyCount = 0, // TODO: Allow dependencies.
                Dependencies = IntPtr.Zero,
            };
            renderPass = device.CreateRenderPass(ref createInfo);
        }

        public FrameBuffer CreateFrameBuffer(Vk.ImageView[] attachments, Vk.Extent2D size)
        {
            Vk.FramebufferCreateInfo createInfo = new Vk.FramebufferCreateInfo
            {
                StructureType = Vk.StructureType.FramebufferCreateInfo,
                RenderPass = renderPass,
                Width = size.Width,
                Height = size.Height,
                AttachmentCount = (uint)attachments.Length,
                Attachments = Marshal.UnsafeAddrOfPinnedArrayElement(attachments, 0),
                Layers = 1,
            };
            Vk.Framebuffer nativeFrameBuffer = device.CreateFramebuffer(ref createInfo);
            FrameBuffer frameBuffer = new FrameBuffer(nativeFrameBuffer, size);
            frameBuffers.Add(frameBuffer, nativeFrameBuffer);
            return frameBuffer;
        }

        public void DestroyFrameBuffer(FrameBuffer buffer)
        {
            device.DestroyFramebuffer(frameBuffers[buffer]);
            frameBuffers.Remove(buffer);
        }

        // TODO: Manage clear values better (based on the attachments).
        public void Begin(FrameBuffer frameBuffer, Vk.CommandBuffer commandBuffer, Vk.ClearValue* clearValues, uint clearValueCount)
        {
            Vk.RenderPassBeginInfo beginInfo = new Vk.RenderPassBeginInfo
            {
                StructureType = Vk.StructureType.RenderPassBeginInfo,
                ClearValueCount = 1,
                ClearValues = (IntPtr)clearValues,
                RenderArea = new Vk.Rect2D(new Vk.Offset2D(0, 0), frameBuffer.Size), // TODO: dsaAdd an option to modify this.
                Framebuffer = frameBuffers[frameBuffer],
                RenderPass = renderPass,
            };
            commandBuffer.BeginRenderPass(ref beginInfo, Vk.SubpassContents.Inline);
        }

        public void Dispose()
        {
            device.DestroyRenderPass(renderPass);
        }
    }
}