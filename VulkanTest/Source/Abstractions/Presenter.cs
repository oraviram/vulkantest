﻿using System;
using SDL2;

using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class Presenter : IDisposable
    {
        private Vk.Surface surface;
        private Vk.Swapchain swapchain;

        public Vk.Extent2D ImageExtent { get; private set; }
        public Vk.Format ImageFormat { get; private set; }
        public Vk.ColorSpace ImageColorSpace { get; private set; }
        public Vk.PresentMode PresentMode { get; private set; }
        private Vk.ImageView[] imageViews;

        public RenderPass RenderPass { get; private set; }
        private FrameBuffer[] frameBuffers;
        public FrameBuffer GetFrameBuffer(int index) => frameBuffers[index];

        private Vk.Instance instance;
        private Devices devices;
        private Window window;
        private Vk.Queue queue;

        /// <summary>
        /// Use <seealso cref="Device.CreatePresenter(Window)"/>
        /// </summary>
        public Presenter(Window window, Vk.Instance instance, Devices devices, uint presentationQueueFamilyIndex)
        {
            this.window = window;
            this.instance = instance;
            this.devices = devices;
            queue = devices.device.GetQueue(presentationQueueFamilyIndex, 0); // TODO: Make compatible for multiple queues when that's allowed.

            CreateSurface();
            if (!devices.physicalDevice.GetSurfaceSupport(presentationQueueFamilyIndex, surface))
                throw new InvalidOperationException("Attempt to create presenter with incompatible device or queue family.");

            CreateSwapchain(true);
            window.OnResized += OnWindowResized;
        }

        private void CreateSurface()
        {
            // TODO: Make platform independent.
            SDL.SDL_SysWMinfo windowInfo = window.SystemWindowInfo;
            Vk.Win32SurfaceCreateInfo createInfo = new Vk.Win32SurfaceCreateInfo
            {
                StructureType = Vk.StructureType.Win32SurfaceCreateInfo,
                WindowHandle = windowInfo.info.win.window,
                InstanceHandle = windowInfo.info.win.hdc,
            };
            surface = instance.CreateWin32Surface(createInfo);
        }

        private void CreateSwapchain(bool firstTime)
        {
            if (firstTime)
            {
                Vk.SurfaceFormat surfaceFormat = ChooseSurfaceFormat(devices.physicalDevice.GetSurfaceFormats(surface));
                ImageFormat = surfaceFormat.Format;
                ImageColorSpace = surfaceFormat.ColorSpace;

                PresentMode = ChoosePresentMode(devices.physicalDevice.GetSurfacePresentModes(surface));
            }
            devices.physicalDevice.GetSurfaceCapabilities(surface, out Vk.SurfaceCapabilities surfaceCapabilities);
            ImageExtent =
                surfaceCapabilities.CurrentExtent.Width != uint.MaxValue ?
                ImageExtent = surfaceCapabilities.CurrentExtent :
                ImageExtent = window.Size.Clamp(surfaceCapabilities.MinImageExtent, surfaceCapabilities.MaxImageExtent);

            Vk.SwapchainCreateInfo createInfo = new Vk.SwapchainCreateInfo
            {
                StructureType = Vk.StructureType.SwapchainCreateInfo,
                OldSwapchain = Vk.Swapchain.Null,
                Surface = surface,

                Clipped = false,
                CompositeAlpha = Vk.CompositeAlphaFlags.Opaque,
                ImageArrayLayers = 1,
                ImageSharingMode = Vk.SharingMode.Exclusive,
                ImageUsage = Vk.ImageUsageFlags.ColorAttachment,
                PreTransform = Vk.SurfaceTransformFlags.Identity,
                QueueFamilyIndexCount = 0,
                QueueFamilyIndices = IntPtr.Zero,

                ImageColorSpace = ImageColorSpace,
                ImageExtent = ImageExtent,
                ImageFormat = ImageFormat,
                PresentMode = PresentMode,
                MinImageCount = CalculateWantedMinImageCount(surfaceCapabilities.MinImageCount, surfaceCapabilities.MaxImageCount),
            };
            swapchain = devices.device.CreateSwapchain(ref createInfo);

            Vk.Image[] images = devices.device.GetSwapchainImages(swapchain);
            if (firstTime)
                RenderPass = new RenderPass(devices.device, ImageFormat, 0);

            frameBuffers = new FrameBuffer[images.Length];
            imageViews = new Vk.ImageView[images.Length];
            for (int i = 0; i < images.Length; i++)
            {
                Vk.ImageViewCreateInfo imageViewCreateInfo = new Vk.ImageViewCreateInfo
                {
                    StructureType = Vk.StructureType.ImageViewCreateInfo,
                    Components = new Vk.ComponentMapping(Vk.ComponentSwizzle.Identity),
                    SubresourceRange = new Vk.ImageSubresourceRange(Vk.ImageAspectFlags.Color, 0, 1, 0, 1),
                    Format = ImageFormat,
                    ViewType = Vk.ImageViewType.Image2D,
                    Image = images[i],
                };
                imageViews[i] = devices.device.CreateImageView(ref imageViewCreateInfo);

                Vk.ImageView[] frameBufferAttachments = new Vk.ImageView[1];
                frameBufferAttachments[0] = imageViews[i]; // TODO: Add depth attachment.
                frameBuffers[i] = RenderPass.CreateFrameBuffer(frameBufferAttachments, ImageExtent);
            }
        }

        private void OnWindowResized()
        {
            queue.WaitIdle();

            DestroySwapchain();
            CreateSwapchain(false);
        }

        private void DestroySwapchain()
        {
            for (int i = 0; i < imageViews.Length; i++)
            {
                devices.device.DestroyImageView(imageViews[i]);
                RenderPass.DestroyFrameBuffer(frameBuffers[i]);
            }
            devices.device.DestroySwapchain(swapchain);
        }

        private static Vk.PresentMode ChoosePresentMode(Vk.PresentMode[] presentModes)
        {
            Vk.PresentMode chosenPresentMode = Vk.PresentMode.Fifo;
            foreach (Vk.PresentMode presentMode in presentModes)
            {
                if (presentMode == Vk.PresentMode.Mailbox)
                    return Vk.PresentMode.Mailbox;

                if (presentMode == Vk.PresentMode.Immediate)
                    chosenPresentMode = Vk.PresentMode.Immediate;
            }
            return chosenPresentMode;
        }

        private static Vk.SurfaceFormat ChooseSurfaceFormat(Vk.SurfaceFormat[] formats)
        {
            if (formats.Length == 1 && formats[0].Format == Vk.Format.Undefined)
                return new Vk.SurfaceFormat { ColorSpace = Vk.ColorSpace.SRgbNonlinear, Format = Vk.Format.R8G8B8A8UNorm };

            foreach (Vk.SurfaceFormat format in formats)
            {
                // TODO: Add more preferences.
                if (format.ColorSpace == Vk.ColorSpace.SRgbNonlinear && format.Format == Vk.Format.R8G8B8A8UNorm)
                    return format;
            }
            return formats[0];
        }

        private static uint CalculateWantedMinImageCount(uint minImageCount, uint maxImageCount)
        {
            uint imageCount = minImageCount + 1;
            if (maxImageCount != uint.MaxValue && imageCount > maxImageCount)
                imageCount = maxImageCount;

            return imageCount;
        }

        public uint AcquireNextImage(Vk.Fence signalFence)
        {
            return devices.device.AcquireNextImage(swapchain, ulong.MaxValue, Vk.Semaphore.Null, signalFence);
        }

        // TODO: Allow multiple wait semaphores.
        public void Present(uint imageIndex, Vk.Semaphore waitSemaphore)
        {
            Vk.Result result;
            fixed (Vk.Swapchain* swapchain = &this.swapchain)
            {
                Vk.PresentInfo presentInfo = new Vk.PresentInfo
                {
                    StructureType = Vk.StructureType.PresentInfo,
                    SwapchainCount = 1,
                    Swapchains = (IntPtr)swapchain,
                    ImageIndices = new IntPtr(&imageIndex),
                    Results = new IntPtr(&result),
                    WaitSemaphoreCount = 1,
                    WaitSemaphores = new IntPtr(&waitSemaphore), // TODO: Add semaphores.
                };
                queue.Present(ref presentInfo);
            }
            Vk.ResultExtensions.CheckError(result);
        }

        public void Dispose()
        {
            DestroySwapchain();
            RenderPass.Dispose();
            instance.DestroySurface(surface);
        }
    }
}