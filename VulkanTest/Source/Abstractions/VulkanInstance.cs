﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Vk = SharpVulkan;

namespace VulkanTest
{
    struct ApplicationInfo
    {
        public Vk.Version Version;
        public string Name;
        public Vk.Version EngineVersion;
        public string EngineName;
    }

    /// <summary>
    /// Represents a vulkan instance.
    /// </summary>
    unsafe class VulkanInstance : IDisposable
    {
        delegate void DebugReportCallbackDelegate(Vk.DebugReportFlags flags, Vk.DebugReportObjectType objectType, ulong obj, Vk.PointerSize location, int code, string layerPrefix, string message, IntPtr userData);
        delegate void CreateDebugReportDelegate(Vk.Instance instance, Vk.DebugReportCallbackCreateInfo* createInfo, Vk.AllocationCallbacks* allocator, Vk.DebugReportCallback* callback);
        delegate void DestroyDebugReportDelegate(Vk.Instance instance, Vk.DebugReportCallback callback, Vk.AllocationCallbacks* allocator);

        private Vk.Instance instance;
        private Vk.DebugReportCallback debugReportCallback;
        private DebugReportCallbackDelegate debugReportCallbackFunctionReference;
        private readonly bool enableDebugReport;

        public VulkanInstance(bool enableDebugReport, ref ApplicationInfo applicationInfo)
        {
            this.enableDebugReport = enableDebugReport;

            Vk.ApplicationInfo vulkanAppInfo = new Vk.ApplicationInfo
            {
                StructureType = Vk.StructureType.ApplicationInfo,
                ApiVersion = Vk.Vulkan.ApiVersion,
                ApplicationVersion = applicationInfo.Version,
                ApplicationName = Marshal.StringToHGlobalAnsi(applicationInfo.Name),
                EngineVersion = applicationInfo.EngineVersion,
                EngineName = Marshal.StringToHGlobalAnsi(applicationInfo.EngineName),
            };

            Vk.InstanceCreateInfo instanceCreateInfo = new Vk.InstanceCreateInfo
            {
                StructureType = Vk.StructureType.InstanceCreateInfo,
                ApplicationInfo = new IntPtr(&vulkanAppInfo),
            };
            FillExtensions(ref instanceCreateInfo);
            FillLayers(ref instanceCreateInfo);
            instance = Vk.Vulkan.CreateInstance(ref instanceCreateInfo);

            Marshal.FreeHGlobal(vulkanAppInfo.ApplicationName);
            Marshal.FreeHGlobal(vulkanAppInfo.EngineName);

            if (enableDebugReport)
                CreateDebugReport();
        }

        void FillExtensions (ref Vk.InstanceCreateInfo instanceCreateInfo)
        {
            List<string> names = new List<string>();
            names.Add("VK_KHR_surface");
            names.Add("VK_KHR_win32_surface"); // TODO: Make platform independent.
            if (enableDebugReport)
                names.Add("VK_EXT_debug_report");

            IntPtr[] namePointers = VulkanUtilities.GetNamePointers(names, Vk.Vulkan.GetInstanceExtensionProperties(), "instance extensions");
            instanceCreateInfo.EnabledExtensionCount = namePointers == null ? 0 : (uint)namePointers.Length;
            instanceCreateInfo.EnabledExtensionNames = namePointers == null ? IntPtr.Zero : Marshal.UnsafeAddrOfPinnedArrayElement(namePointers, 0);
        }

        void FillLayers(ref Vk.InstanceCreateInfo instanceCreateInfo)
        {
            List<string> names = new List<string>();
            if (enableDebugReport)
                names.Add("VK_LAYER_LUNARG_standard_validation");

            IntPtr[] namePointers = VulkanUtilities.GetNamePointers(names, Vk.Vulkan.InstanceLayerProperties, "instance layers");
            instanceCreateInfo.EnabledLayerCount = namePointers == null ? 0 : (uint)namePointers.Length;
            instanceCreateInfo.EnabledLayerNames = namePointers == null ? IntPtr.Zero : Marshal.UnsafeAddrOfPinnedArrayElement(namePointers, 0);
        }

        void CreateDebugReport()
        {
            VulkanUtilities.CallFunctionOnInstance<CreateDebugReportDelegate>(instance, "vkCreateDebugReportCallbackEXT", (func) =>
            {
                debugReportCallbackFunctionReference = new DebugReportCallbackDelegate(Callback);
                Vk.DebugReportCallbackCreateInfo createInfo = new Vk.DebugReportCallbackCreateInfo
                {
                    StructureType = Vk.StructureType.DebugReportCallbackCreateInfo,
                    Flags = (uint)(Vk.DebugReportFlags.Error | Vk.DebugReportFlags.Information | Vk.DebugReportFlags.PerformanceWarning | Vk.DebugReportFlags.Warning),
                    Callback = Marshal.GetFunctionPointerForDelegate(debugReportCallbackFunctionReference),
                    UserData = IntPtr.Zero,
                };
                fixed (Vk.DebugReportCallback* debugReportCallbackPointer = &debugReportCallback)
                    func(instance, &createInfo, null, debugReportCallbackPointer);

            }, "Failed to create debug report.");

            void Callback(Vk.DebugReportFlags flags, Vk.DebugReportObjectType objectType, ulong obj, Vk.PointerSize location, int code, string layerPrefix, string message, IntPtr userData)
            {
                switch (flags)
                {
                    case Vk.DebugReportFlags.Error:
                        Logger.Log(string.Format("VULKAN ERROR ({0}): ", objectType) + message, ConsoleColor.Red);
                        break;

                    case Vk.DebugReportFlags.Warning:
                        Logger.Log(string.Format("VULKAN WARNING ({0}): ", objectType) + message, ConsoleColor.Yellow);
                        break;

                    case Vk.DebugReportFlags.PerformanceWarning:
                        Logger.Log(string.Format("VULKAN PERFORMANCE WARNING ({0}): ", objectType) + message, ConsoleColor.Green);
                        break;

                    case Vk.DebugReportFlags.Information:
                        Logger.Log(string.Format("VULKAN INFORMATION ({0}): ", objectType) + message, ConsoleColor.Cyan);
                        break;

                    case Vk.DebugReportFlags.Debug:
                        Logger.Log(string.Format("VULKAN DEBUG ({0}): ", objectType) + message, ConsoleColor.Gray);
                        break;
                }
            }
        }

        /// <summary>
        /// Creates a device with this instance and returns it. The device is NOT owned by this instance.
        /// </summary>
        public Device CreateDevice()
        {
            return new Device(instance, enableDebugReport);
        }

        public void Dispose()
        {
            if (enableDebugReport)
                VulkanUtilities.CallFunctionOnInstance<DestroyDebugReportDelegate>(instance, "vkDestroyDebugReportCallbackEXT", func => func(instance, debugReportCallback, null), "Validation layers extension missing!");

            instance.Destroy();
        }
    }
}