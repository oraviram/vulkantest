﻿using System;
using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class FrameBuffer
    {
        private Vk.Framebuffer frameBuffer;
        public Vk.Extent2D Size { get; private set; }

        public FrameBuffer(Vk.Framebuffer frameBuffer, Vk.Extent2D size)
        {
            this.frameBuffer = frameBuffer;
            Size = size;
        }
    }
}