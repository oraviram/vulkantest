﻿using System;
using SDL2;

using Vk = SharpVulkan;

namespace VulkanTest
{
    unsafe class Window : IDisposable
    {
        private IntPtr _handle;
        public IntPtr Handle
        {
            get { return _handle; }
        }

        public Vk.Extent2D Size
        {
            get
            {
                SDL.SDL_GetWindowSize(Handle, out int width, out int height);
                return new Vk.Extent2D((uint)width, (uint)height);
            }
            set
            {
                SDL.SDL_SetWindowSize(Handle, (int)value.Width, (int)value.Height);
            }
        }

        public string Title
        {
            get => SDL.SDL_GetWindowTitle(Handle);
            set => SDL.SDL_SetWindowTitle(Handle, value);
        }

        public SDL.SDL_SysWMinfo SystemWindowInfo
        {
            get
            {
                SDL.SDL_SysWMinfo windowInfo = new SDL.SDL_SysWMinfo();
                SDL.SDL_GetWindowWMInfo(Handle, ref windowInfo);
                return windowInfo;
            }
        }

        public event Action OnResized;
        public event Action OnUpdate;

        public Window(string title, int width, int height, SDL.SDL_WindowFlags flags)
        {
            _handle = SDL.SDL_CreateWindow(title, SDL.SDL_WINDOWPOS_UNDEFINED, SDL.SDL_WINDOWPOS_UNDEFINED, width, height, flags);
        }

        public void EnterLoop()
        {
            bool running = true;
            while (running)
            {
                OnUpdate?.Invoke();
                while (SDL.SDL_PollEvent(out SDL.SDL_Event e) != 0)
                {
                    if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    {
                        running = false;
                        break;
                    }
                    else if (e.window.windowEvent == SDL.SDL_WindowEventID.SDL_WINDOWEVENT_RESIZED)
                        OnResized?.Invoke();
                }
            }
        }

        public void Dispose()
        {
            SDL.SDL_DestroyWindow(Handle);
        }
    }
}