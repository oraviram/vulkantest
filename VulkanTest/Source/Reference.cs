﻿// This file is for a reference in the actual project.

/*

using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SDL2;
using SharpVulkan;

using Version = SharpVulkan.Version;
using Buffer = SharpVulkan.Buffer;
using Image = SharpVulkan.Image;

namespace VulkanTest
{
    unsafe struct Shader
    {
        public readonly ShaderModule Module;
        public readonly ShaderStageFlags Stage;

        public Shader(ShaderModule module, ShaderStageFlags stage)
        {
            Module = module;
            Stage = stage;
        }

        public void Destroy(Device device)
        {
            device.DestroyShaderModule(Module);
        }
    }

    unsafe struct BufferResource
    {
        public readonly ulong Size;
        public readonly Buffer Buffer;
        public readonly DeviceMemory Memory;

        public BufferResource(ulong size, Buffer buffer, DeviceMemory memory)
        {
            Size = size;
            Buffer = buffer;
            Memory = memory;
        }

        public void Destroy(Device device)
        {
            device.FreeMemory(Memory);
            device.DestroyBuffer(Buffer);
        }
    }

    unsafe struct ImageResource
    {
        public readonly Image Image;
        public readonly ImageView ImageView;
        public readonly DeviceMemory Memory;
        public readonly Format Format;
        public readonly ImageAspectFlags Aspect;
        public readonly Extent2D Extent;
        public ImageLayout LastSetLayout;

        public ImageResource(Image image, ImageView imageView, DeviceMemory memory, Format format, ImageAspectFlags aspect, Extent2D extent)
        {
            Image = image;
            ImageView = imageView;
            Memory = memory;
            Aspect = aspect;
            Format = format;
            Extent = extent;

            LastSetLayout = ImageLayout.Undefined;
        }

        public void Destroy(Device device)
        {
            device.FreeMemory(Memory);
            device.DestroyImageView(ImageView);
            device.DestroyImage(Image);
        }
    }

    unsafe struct Texture
    {
        public readonly ImageResource Image;
        public readonly Sampler Sampler;

        public Texture(ImageResource image, Sampler sampler)
        {
            Image = image;
            Sampler = sampler;
        }

        public void Destroy(Device device)
        {
            Image.Destroy(device);
            device.DestroySampler(Sampler);
        }
    }

    struct Vertex
    {
        public Vector3 Position;
        public Vector4 Color;
        public Vector2 TextureCoordinate;
    }

    struct MVPMatrices
    {
        public static readonly MVPMatrices Identity = new MVPMatrices { ModelMatrix = Matrix4x4.Identity, ViewMatrix = Matrix4x4.Identity, ProjectionMatrix = Matrix4x4.Identity };

        public Matrix4x4 ModelMatrix;
        public Matrix4x4 ViewMatrix;
        public Matrix4x4 ProjectionMatrix;
    }

    unsafe static class Program
    {
        const int QUEUE_FAMILY_INDEX_INVALID = -1;
        const string SHADER_ENTRY_POINT_FUNCTION_NAME = "main";
        const float MIN_TIME_SINCE_LAST_FRAME = 0.000001f;

        delegate void DebugReportCallbackDelegate(DebugReportFlags flags, DebugReportObjectType objectType, ulong obj, PointerSize location, int code, string layerPrefix, string message, IntPtr userData);
        delegate void CreateDebugReportDelegate(Instance instance, DebugReportCallbackCreateInfo* createInfo, AllocationCallbacks* allocator, DebugReportCallback* callback);
        delegate void DestroyDebugReportDelegate(Instance instance, DebugReportCallback callback, AllocationCallbacks* allocator);

        static Instance instance;
        static DebugReportCallback debugReport;
        static PhysicalDevice physicalDevice;
        static Device device;
        static int graphicsQueueFamilyIndex = QUEUE_FAMILY_INDEX_INVALID;
        static int presentationQueueFamilyIndex = QUEUE_FAMILY_INDEX_INVALID;
        static int transferQueueFamilyIndex = QUEUE_FAMILY_INDEX_INVALID;
        static Dictionary<int, Queue> deviceQueues;

        static IntPtr window;
        static Surface surface;
        static Swapchain swapchain;
        static SurfaceFormat swapchainSurfaceFormat;
        static PresentMode swapchainPresentMode = PresentMode.Fifo;
        static Extent2D swapchainImageExtent;
        static ImageView[] imageViews;
        static RenderPass renderPass;

        static Shader[] shaders;
        static PipelineLayout pipelineLayout;
        static Pipeline pipeline;

        const int MAX_UNIFORMS = 1;
        const int MAX_TEXTURES = 1;
        static DescriptorPool descriptorPool;
        static DescriptorSetLayout descriptorSetLayout;
        static DescriptorSet descriptorSet;

        static CommandPool graphicsCommandPool;
        static CommandPool transferCommandPool;

        static Framebuffer[] frameBuffers;
        static CommandBuffer[] commandBuffers;

        static Semaphore aquiredImageSemaphore;
        static Semaphore presentationReadySemaphore;

        static BufferResource vertexBuffer;
        static BufferResource indexBuffer;
        static BufferResource uniformBuffer;

        static ImageResource depthImage;
        static Texture crateTexture;

        static readonly MVPMatrices[] mvpMatricesBufferData = new MVPMatrices[1] { MVPMatrices.Identity };
        static ref MVPMatrices mvpMatrices => ref mvpMatricesBufferData[0];

        // NOTE: It's currently not a cube just for testing.
        static readonly CullModeFlags cullMode = CullModeFlags.Back;
        static Vector4 ClearColor { get; } = new Vector4(0, 0, 0, 1);
        static Vector4 Tint { get; } = new Vector4(2, 1.33f, 1, 1);
        static float rotationSpeed = 1000;
        static Vertex[] vertexInformation = new Vertex[]
        {
            // Back:
            new Vertex { Position = new Vector3(-1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(-1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },
            
            // Front:
            new Vertex { Position = new Vector3(-1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(-1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },

            // Top:
            new Vertex { Position = new Vector3(-1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(-1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },
            
            // Bottom:
            new Vertex { Position = new Vector3(-1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(-1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },

            // Left:
            new Vertex { Position = new Vector3(1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },

            // Right:
            new Vertex { Position = new Vector3(-1, -1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 0) },
            new Vertex { Position = new Vector3(-1, -1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(0, 1) },
            new Vertex { Position = new Vector3(-1, 1, 1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 0) },
            new Vertex { Position = new Vector3(-1, 1, -1), Color = new Vector4(1, 1, 1, 1), TextureCoordinate = new Vector2(1, 1) },

        };
        static uint[] vertexIndices = new uint[]
        {
            0, 1, 2, 2, 1, 3, // Back
            4, 5, 6, 6, 5, 7, // Front
            8, 9, 10, 10, 9, 11, // Top
            12, 13, 14, 14, 13, 15, // Bottom
            16, 17, 18, 18, 17, 19, // Left
            20, 21, 22, 22, 21, 23, // Right
        };

        static Stopwatch timeRunningWatch;
        static float timeSinceLastFrame;

        static string[] instanceExtensions = new string[] { "VK_EXT_debug_report", "VK_KHR_surface", "VK_KHR_win32_surface" };
        static string[] instanceValidationLayers = new string[] { "VK_LAYER_LUNARG_standard_validation" };
        static string[] deviceExtensions = new string[] { "VK_KHR_swapchain" };

        static void Main(string[] args)
        {
            timeRunningWatch = new Stopwatch();

            SDL.SDL_Init(SDL.SDL_INIT_VIDEO);

            window = SDL.SDL_CreateWindow("Vulkan Sandbox", SDL.SDL_WINDOWPOS_UNDEFINED, SDL.SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN | SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE);
            InitializeVulkan();

            bool running = true;
            timeRunningWatch.Start();
            float oldTime = 0;
            while (running)
            {
                timeSinceLastFrame = Math.Min((float)timeRunningWatch.Elapsed.TotalSeconds - oldTime, MIN_TIME_SINCE_LAST_FRAME);

                UpdateApplication();
                DrawFrame();
                while (SDL.SDL_PollEvent(out SDL.SDL_Event e) != 0)
                {
                    if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    {
                        running = false;
                        break;
                    }
                    else if (e.window.windowEvent == SDL.SDL_WindowEventID.SDL_WINDOWEVENT_RESIZED)
                        RecreateSwapchain();
                }
                oldTime = (float)timeRunningWatch.Elapsed.TotalSeconds;
            }
            timeRunningWatch.Stop();

            DeInitializeVulkan();
            SDL.SDL_DestroyWindow(window);
            SDL.SDL_Quit();

            // (I want to see the logs at the end.)
            Console.ReadKey(true);
        }

        static void InitializeVulkan()
        {
            CreateInstance();
            CreateDebugReport();

            CreateSurface();
            ChoosePhysicalDevice();
            CreateLogicalDevice();

            CreateSwapchain();
            CreateCommandPools();

            uniformBuffer = CreateBuffer((ulong)Marshal.SizeOf<MVPMatrices>(), BufferUsageFlags.UniformBuffer, MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);
            CreateDepthImage();
            crateTexture = LoadTexture("Textures/Crate.jpg", 1);

            CreateDescriptorPool();
            CreateDescriptorSetLayout();
            AllocateDescriptorSets();

            CreateRenderPass();
            shaders = new Shader[]
            {
                LoadShader("Shaders/vert.spv", ShaderStageFlags.Vertex),
                LoadShader("Shaders/frag.spv", ShaderStageFlags.Fragment),
            };
            CreatePipeline();

            ulong vertexBufferSize = (ulong)(vertexInformation.Length * Marshal.SizeOf<Vertex>());
            ulong indexBufferSize = (ulong)vertexIndices.Length * sizeof(uint);

            vertexBuffer = CreateBuffer(vertexBufferSize, BufferUsageFlags.VertexBuffer | BufferUsageFlags.TransferDestination, MemoryPropertyFlags.DeviceLocal);
            indexBuffer = CreateBuffer(indexBufferSize, BufferUsageFlags.IndexBuffer | BufferUsageFlags.TransferDestination, MemoryPropertyFlags.DeviceLocal);

            BufferResource stagingBuffer = CreateBuffer(vertexBufferSize, BufferUsageFlags.TransferSource, MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);
            SetBufferData(stagingBuffer, vertexInformation);
            CopyBuffer(stagingBuffer, vertexBuffer);
            stagingBuffer.Destroy(device);

            stagingBuffer = CreateBuffer(indexBufferSize, BufferUsageFlags.TransferSource, MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);
            SetBufferData(stagingBuffer, vertexIndices);
            CopyBuffer(stagingBuffer, indexBuffer);
            stagingBuffer.Destroy(device);

            CreateFrameBuffers();
            AllocateGraphicsCommandBuffers();

            SemaphoreCreateInfo semaphoreCreateInfo = new SemaphoreCreateInfo { StructureType = StructureType.SemaphoreCreateInfo };
            aquiredImageSemaphore = device.CreateSemaphore(ref semaphoreCreateInfo);
            presentationReadySemaphore = device.CreateSemaphore(ref semaphoreCreateInfo);
        }

        static void CreateInstance()
        {
            IntPtr[] enabledExtensionNames = Memory.GetNamePointers(instanceExtensions, Vulkan.GetInstanceExtensionProperties(), "device extensions");
            IntPtr[] enabledLayerNames = Memory.GetNamePointers(instanceValidationLayers, Vulkan.InstanceLayerProperties, "validation layers");

            fixed (void* extensionNamesPointers = &enabledExtensionNames[0])
            fixed (void* layerNamesPointers = &enabledLayerNames[0])
            {
                ApplicationInfo appInfo = new ApplicationInfo
                {
                    StructureType = StructureType.ApplicationInfo,
                    ApiVersion = Vulkan.ApiVersion,
                    EngineVersion = new Version(1, 0, 0),
                    // And screw the rest!
                };
                InstanceCreateInfo createInfo = new InstanceCreateInfo
                {
                    StructureType = StructureType.InstanceCreateInfo,
                    ApplicationInfo = (IntPtr)(&appInfo),
                    EnabledExtensionCount = (uint)instanceExtensions.Length,
                    EnabledExtensionNames = (IntPtr)extensionNamesPointers,
                    EnabledLayerCount = (uint)instanceValidationLayers.Length,
                    EnabledLayerNames = (IntPtr)layerNamesPointers,
                };
                instance = Vulkan.CreateInstance(ref createInfo);
            }
        }

        static void CreateDebugReport()
        {
            CallFunctionOnInstance<CreateDebugReportDelegate>("vkCreateDebugReportCallbackEXT",
                func =>
                {
                    DebugReportCallbackCreateInfo createInfo = new DebugReportCallbackCreateInfo
                    {
                        StructureType = StructureType.DebugReportCallbackCreateInfo,
                        Flags = (uint)(DebugReportFlags.Error | DebugReportFlags.Information | DebugReportFlags.PerformanceWarning | DebugReportFlags.Warning),
                        Callback = Marshal.GetFunctionPointerForDelegate(new DebugReportCallbackDelegate(Callback)),
                    };
                    fixed (DebugReportCallback* debugReportPtr = &debugReport)
                        func(instance, &createInfo, null, debugReportPtr);

                }, "Validation layers extension missing!");

            void Callback(DebugReportFlags flags, DebugReportObjectType objectType, ulong obj, PointerSize location, int code, string layerPrefix, string message, IntPtr userData)
            {
                switch (flags)
                {
                    case DebugReportFlags.Error:
                        Logger.Log("VULKAN ERROR: " + message, ConsoleColor.Red);
                        break;

                    case DebugReportFlags.Warning:
                        Logger.Log("VULKAN WARNING: " + message, ConsoleColor.Yellow);
                        break;

                    case DebugReportFlags.PerformanceWarning:
                        Logger.Log("VULKAN PERFORMANCE WARNING: " + message, ConsoleColor.Green);
                        break;

                    case DebugReportFlags.Information:
                        Logger.Log("VULKAN INFORMATION: " + message, ConsoleColor.Cyan);
                        break;

                    case DebugReportFlags.Debug:
                        Logger.Log("VULKAN DEBUG: " + message, ConsoleColor.Gray);
                        break;
                }
            }
        }

        static void CreateSurface()
        {
            SDL.SDL_SysWMinfo windowInfo = new SDL.SDL_SysWMinfo();
            SDL.SDL_GetWindowWMInfo(window, ref windowInfo);
            Win32SurfaceCreateInfo createInfo = new Win32SurfaceCreateInfo
            {
                StructureType = StructureType.Win32SurfaceCreateInfo,
                WindowHandle = windowInfo.info.win.window,
                InstanceHandle = windowInfo.info.win.hdc,
            };
            surface = instance.CreateWin32Surface(createInfo);
        }

        static void ChoosePhysicalDevice()
        {
            PhysicalDevice[] availableDevices = instance.PhysicalDevices;
            physicalDevice = PhysicalDevice.Null;
            int bestScore = 0;
            foreach (PhysicalDevice device in availableDevices)
            {
                int score = RatePhysicalDevice(device);
                if (bestScore < score)
                {
                    bestScore = score;
                    physicalDevice = device;
                }
            }
            if (physicalDevice == PhysicalDevice.Null)
                throw new Exception("Could not find fitting physical device!");
        }

        static int RatePhysicalDevice(PhysicalDevice device)
        {
            int score = 0;

            device.GetFeatures(out PhysicalDeviceFeatures features);
            if (!features.SamplerAnisotropy)
                return 0;

            device.GetProperties(out PhysicalDeviceProperties deviceProperties);
            QueueFamilyProperties[] queueFamilyPropertiesArray = device.QueueFamilyProperties;
            for (uint i = 0; i < queueFamilyPropertiesArray.Length; i++)
            {
                QueueFamilyProperties queueFamilyProperties = queueFamilyPropertiesArray[i];
                if (queueFamilyProperties.QueueFlags.HasFlag(QueueFlags.Graphics))
                    graphicsQueueFamilyIndex = (int)i;
                else if (queueFamilyProperties.QueueFlags.HasFlag(QueueFlags.Transfer))
                    transferQueueFamilyIndex = (int)i;

                if (device.GetSurfaceSupport(i, surface))
                    presentationQueueFamilyIndex = (int)i;
            }
            if (graphicsQueueFamilyIndex == QUEUE_FAMILY_INDEX_INVALID || presentationQueueFamilyIndex == QUEUE_FAMILY_INDEX_INVALID)
                return 0;

            if (transferQueueFamilyIndex == QUEUE_FAMILY_INDEX_INVALID)
                transferQueueFamilyIndex = graphicsQueueFamilyIndex;

            if (graphicsQueueFamilyIndex == presentationQueueFamilyIndex)
                score += 5;

            switch (deviceProperties.DeviceType)
            {
                case PhysicalDeviceType.DiscreteGpu:
                    score += 5; break;

                case PhysicalDeviceType.VirtualGpu:
                    score += 4; break;

                case PhysicalDeviceType.IntegratedGpu:
                    score += 3; break;

                case PhysicalDeviceType.Cpu:
                    score += 2; break;

                case PhysicalDeviceType.Other:
                    score += 1; break;
            }
            return score;
        }

        static void CreateLogicalDevice()
        {
            int queueFamiliesCount = graphicsQueueFamilyIndex == presentationQueueFamilyIndex ? 1 : 2;
            if (graphicsQueueFamilyIndex != transferQueueFamilyIndex)
                queueFamiliesCount++;

            uint* queueFamilyIndices = stackalloc uint[3];
            queueFamilyIndices[0] = (uint)graphicsQueueFamilyIndex;
            queueFamilyIndices[1] = (uint)presentationQueueFamilyIndex;
            queueFamilyIndices[2] = (uint)transferQueueFamilyIndex;

            float* priorities = stackalloc float[1];
            priorities[0] = 1;

            DeviceQueueCreateInfo* queueCreateInfos = stackalloc DeviceQueueCreateInfo[queueFamiliesCount];
            for (int i = 0; i < queueFamiliesCount; i++)
            {
                queueCreateInfos[i] = new DeviceQueueCreateInfo
                {
                    StructureType = StructureType.DeviceQueueCreateInfo,
                    QueueFamilyIndex = queueFamilyIndices[i],
                    QueueCount = 1,
                    QueuePriorities = (IntPtr)priorities,
                };
            }

            IntPtr[] enabledExtensionNames = Memory.GetNamePointers(deviceExtensions, physicalDevice.GetDeviceExtensionProperties(), "device extensions");
            fixed (void* enabledExtensionNamesPointer = &enabledExtensionNames[0])
            {
                PhysicalDeviceFeatures features = new PhysicalDeviceFeatures
                {
                    SamplerAnisotropy = true,
                };

                DeviceCreateInfo createInfo = new DeviceCreateInfo
                {
                    StructureType = StructureType.DeviceCreateInfo,
                    EnabledExtensionCount = (uint)deviceExtensions.Length,
                    EnabledExtensionNames = (IntPtr)enabledExtensionNamesPointer,
                    EnabledLayerCount = 0,
                    EnabledLayerNames = IntPtr.Zero,
                    EnabledFeatures = new IntPtr(&features),
                    QueueCreateInfoCount = (uint)queueFamiliesCount,
                    QueueCreateInfos = (IntPtr)queueCreateInfos,
                };
                device = physicalDevice.CreateDevice(ref createInfo);
            }

            deviceQueues = new Dictionary<int, Queue>(queueFamiliesCount)
            {
                [graphicsQueueFamilyIndex] = device.GetQueue((uint)graphicsQueueFamilyIndex, 0),
                [presentationQueueFamilyIndex] = device.GetQueue((uint)presentationQueueFamilyIndex, 0),
                [transferQueueFamilyIndex] = device.GetQueue((uint)transferQueueFamilyIndex, 0),
            };
        }

        static void CreateSwapchain()
        {
            PresentMode[] supportedPresentModes = physicalDevice.GetSurfacePresentModes(surface);
            foreach (PresentMode presentMode in supportedPresentModes)
            {
                if (presentMode == PresentMode.Mailbox)
                {
                    swapchainPresentMode = presentMode;
                    break;
                }
                if (presentMode == PresentMode.Immediate)
                    swapchainPresentMode = presentMode;
            }

            SurfaceFormat[] supportedFormats = physicalDevice.GetSurfaceFormats(surface);
            swapchainSurfaceFormat = supportedFormats[0];
            if (supportedFormats.Length == 1 && supportedFormats[0].Format == Format.Undefined)
                swapchainSurfaceFormat = new SurfaceFormat { Format = Format.R8G8B8A8UNorm, ColorSpace = ColorSpace.SRgbNonlinear };
            else
            {
                foreach (SurfaceFormat format in supportedFormats)
                {
                    if (format.Format == Format.R8G8B8A8UNorm && format.ColorSpace == ColorSpace.SRgbNonlinear)
                    {
                        swapchainSurfaceFormat = format;
                        break;
                    }
                }
            }

            physicalDevice.GetSurfaceCapabilities(surface, out SurfaceCapabilities surfaceCapabilities);
            if (surfaceCapabilities.CurrentExtent.Width != uint.MaxValue)
                swapchainImageExtent = surfaceCapabilities.CurrentExtent;
            else
            {
                SDL.SDL_GetWindowSize(window, out int width, out int height);
                swapchainImageExtent = new Extent2D
                {
                    Width = MathUtils.Clamp((uint)width, surfaceCapabilities.MinImageExtent.Width, surfaceCapabilities.MaxImageExtent.Width),
                    Height = MathUtils.Clamp((uint)height, surfaceCapabilities.MinImageExtent.Height, surfaceCapabilities.MaxImageExtent.Height),
                };
            }
            uint minImageCount = surfaceCapabilities.MinImageCount + 1;
            if (surfaceCapabilities.MaxImageCount != 0 && minImageCount > surfaceCapabilities.MaxImageCount)
                minImageCount = surfaceCapabilities.MaxImageCount;
            
            SwapchainCreateInfo createInfo = new SwapchainCreateInfo
            {
                StructureType = StructureType.SwapchainCreateInfo,
                Surface = surface,
                QueueFamilyIndexCount = (uint)deviceQueues.Count,
                QueueFamilyIndices = Marshal.UnsafeAddrOfPinnedArrayElement(deviceQueues.Keys.ToArray(), 0),
                
                Clipped = false,
                PreTransform = SurfaceTransformFlags.Identity,
                CompositeAlpha = CompositeAlphaFlags.Opaque,
                ImageUsage = ImageUsageFlags.ColorAttachment,
                ImageSharingMode = deviceQueues.Count == 1 ? SharingMode.Exclusive : SharingMode.Concurrent,
                ImageArrayLayers = 1,

                PresentMode = swapchainPresentMode,
                ImageFormat = swapchainSurfaceFormat.Format,
                ImageColorSpace = swapchainSurfaceFormat.ColorSpace,
                ImageExtent = swapchainImageExtent,
                MinImageCount = minImageCount,
            };
            swapchain = device.CreateSwapchain(ref createInfo);

            Image[] images = device.GetSwapchainImages(swapchain);
            imageViews = new ImageView[images.Length];
            for (int i = 0; i < imageViews.Length; i++)
            {
                ImageViewCreateInfo imageViewCreateInfo = new ImageViewCreateInfo
                {
                    StructureType = StructureType.ImageViewCreateInfo,
                    Format = swapchainSurfaceFormat.Format,
                    Image = images[i],
                    ViewType = ImageViewType.Image2D,
                    Components = new ComponentMapping(ComponentSwizzle.Identity),
                    SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.Color, 0, 1, 0, 1),
                };
                imageViews[i] = device.CreateImageView(ref imageViewCreateInfo);
            }
        }

        static void CreateCommandPools()
        {
            CommandPoolCreateInfo graphicsCreateInfo = new CommandPoolCreateInfo
            {
                StructureType = StructureType.CommandPoolCreateInfo,
                Flags = CommandPoolCreateFlags.ResetCommandBuffer,
                QueueFamilyIndex = (uint)graphicsQueueFamilyIndex,
            };
            graphicsCommandPool = device.CreateCommandPool(ref graphicsCreateInfo);

            if (transferQueueFamilyIndex != graphicsQueueFamilyIndex)
            {
                CommandPoolCreateInfo transferCreateInfo = new CommandPoolCreateInfo
                {
                    StructureType = StructureType.CommandPoolCreateInfo,
                    Flags = CommandPoolCreateFlags.ResetCommandBuffer,
                    QueueFamilyIndex = (uint)transferQueueFamilyIndex,
                };
                transferCommandPool = device.CreateCommandPool(ref transferCreateInfo);
            }
            else
                transferCommandPool = graphicsCommandPool;
        }

        static void CreateDescriptorPool()
        {
            DescriptorPoolSize* sizes = stackalloc DescriptorPoolSize[2];
            sizes[0] = new DescriptorPoolSize
            {
                DescriptorCount = MAX_UNIFORMS,
                Type = DescriptorType.UniformBuffer,
            };
            sizes[1] = new DescriptorPoolSize
            {
                DescriptorCount = MAX_TEXTURES,
                Type = DescriptorType.CombinedImageSampler,
            };

            DescriptorPoolCreateInfo createInfo = new DescriptorPoolCreateInfo
            {
                StructureType = StructureType.DescriptorPoolCreateInfo,
                MaxSets = 1,
                PoolSizeCount = 2,
                PoolSizes = (IntPtr)sizes,
            };
            descriptorPool = device.CreateDescriptorPool(ref createInfo);
        }

        static void CreateDescriptorSetLayout()
        {
            DescriptorSetLayoutBinding* bindings = stackalloc DescriptorSetLayoutBinding[2];
            bindings[0] = new DescriptorSetLayoutBinding
            {
                Binding = 0,
                DescriptorCount = 1,
                DescriptorType = DescriptorType.UniformBuffer,
                StageFlags = ShaderStageFlags.Vertex,
                ImmutableSamplers = IntPtr.Zero,
            };
            bindings[1] = new DescriptorSetLayoutBinding
            {
                Binding = 1,
                DescriptorCount = 1,
                DescriptorType = DescriptorType.CombinedImageSampler,
                StageFlags = ShaderStageFlags.Fragment,
                ImmutableSamplers = IntPtr.Zero,
            };

            DescriptorSetLayoutCreateInfo createInfo = new DescriptorSetLayoutCreateInfo
            {
                StructureType = StructureType.DescriptorSetLayoutCreateInfo,
                BindingCount = 2,
                Bindings = (IntPtr)bindings,
            };
            descriptorSetLayout = device.CreateDescriptorSetLayout(ref createInfo);
        }

        static void AllocateDescriptorSets()
        {
            fixed (DescriptorSetLayout* setLayouts = &descriptorSetLayout)
            fixed (DescriptorSet* set = &descriptorSet)
            {
                DescriptorSetAllocateInfo allocateInfo = new DescriptorSetAllocateInfo
                {
                    StructureType = StructureType.DescriptorSetAllocateInfo,
                    DescriptorPool = descriptorPool,
                    DescriptorSetCount = 1,
                    SetLayouts = (IntPtr)setLayouts,
                };
                device.AllocateDescriptorSets(ref allocateInfo, set);
            }

            DescriptorBufferInfo uniformBufferInfo = new DescriptorBufferInfo
            {
                Buffer = uniformBuffer.Buffer,
                Offset = 0,
                Range = Vulkan.WholeSize,
            };

            DescriptorImageInfo textureImageInfo = new DescriptorImageInfo
            {
                ImageLayout = ImageLayout.ShaderReadOnlyOptimal,
                ImageView = crateTexture.Image.ImageView,
                Sampler = crateTexture.Sampler,
            };

            WriteDescriptorSet* writes = stackalloc WriteDescriptorSet[2];
            writes[0] = new WriteDescriptorSet
            {
                StructureType = StructureType.WriteDescriptorSet,
                DescriptorCount = 1,
                DescriptorType = DescriptorType.UniformBuffer,
                DestinationSet = descriptorSet,
                DestinationArrayElement = 0,
                DestinationBinding = 0,
                BufferInfo = new IntPtr(&uniformBufferInfo),
                ImageInfo = IntPtr.Zero,
                TexelBufferView = IntPtr.Zero,
            };
            writes[1] = new WriteDescriptorSet
            {
                StructureType = StructureType.WriteDescriptorSet,
                DescriptorCount = 1,
                DescriptorType = DescriptorType.CombinedImageSampler,
                DestinationSet = descriptorSet,
                DestinationArrayElement = 0,
                DestinationBinding = 1,
                BufferInfo = IntPtr.Zero,
                ImageInfo = new IntPtr(&textureImageInfo),
                TexelBufferView = IntPtr.Zero,
            };
            device.UpdateDescriptorSets(2, writes, 0, null);
        }

        static void CreateDepthImage()
        {
            depthImage = CreateImage(swapchainImageExtent, Format.D32SFloat, ImageTiling.Optimal, ImageUsageFlags.DepthStencilAttachment, ImageAspectFlags.Depth, MemoryPropertyFlags.DeviceLocal);
            ChangeImageLayout(ref depthImage, ImageLayout.DepthStencilAttachmentOptimal);
        }

        static void CreateRenderPass()
        {
            AttachmentDescription* attachments = stackalloc AttachmentDescription[2];
            attachments[0] = new AttachmentDescription
            {
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.PresentSource,
                LoadOperation = AttachmentLoadOperation.Clear,
                StoreOperation = AttachmentStoreOperation.Store,
                StencilLoadOperation = AttachmentLoadOperation.DontCare,
                StencilStoreOperation = AttachmentStoreOperation.DontCare,
                Samples = SampleCountFlags.Sample1,
                Format = swapchainSurfaceFormat.Format,
            };
            attachments[1] = new AttachmentDescription
            {
                InitialLayout = ImageLayout.DepthStencilAttachmentOptimal,
                FinalLayout = ImageLayout.DepthStencilAttachmentOptimal,
                LoadOperation = AttachmentLoadOperation.Clear,
                StoreOperation = AttachmentStoreOperation.Store,
                StencilLoadOperation = AttachmentLoadOperation.Clear,
                StencilStoreOperation = AttachmentStoreOperation.Store,
                Samples = SampleCountFlags.Sample1,
                Format = depthImage.Format,
            };

            AttachmentReference* attachmentReferences = stackalloc AttachmentReference[2];
            attachmentReferences[0] = new AttachmentReference
            {
                Attachment = 0,
                Layout = ImageLayout.ColorAttachmentOptimal,
            };
            attachmentReferences[1] = new AttachmentReference
            {
                Attachment = 1,
                Layout = ImageLayout.DepthStencilAttachmentOptimal,
            };

            SubpassDescription* subpasses = stackalloc SubpassDescription[1];
            subpasses[0] = new SubpassDescription
            {
                PipelineBindPoint = PipelineBindPoint.Graphics,
                ColorAttachmentCount = 1,
                ColorAttachments = (IntPtr)attachmentReferences,
                ResolveAttachments = IntPtr.Zero,
                InputAttachmentCount = 0,
                InputAttachments = IntPtr.Zero,
                DepthStencilAttachment = (IntPtr)attachmentReferences + Marshal.SizeOf<AttachmentReference>(),
                PreserveAttachmentCount = 0,
                PreserveAttachments = IntPtr.Zero,
            };

            RenderPassCreateInfo createInfo = new RenderPassCreateInfo
            {
                StructureType = StructureType.RenderPassCreateInfo,
                SubpassCount = 1,
                Subpasses = (IntPtr)subpasses,
                DependencyCount = 0,
                Dependencies = IntPtr.Zero,
                AttachmentCount = 2,
                Attachments = (IntPtr)attachments,
            };
            renderPass = device.CreateRenderPass(ref createInfo);
        }

        static void CreatePipeline()
        {
            fixed (DescriptorSetLayout* setLayouts = &descriptorSetLayout)
            {
                PushConstantRange* pushConstantRanges = stackalloc PushConstantRange[1];
                pushConstantRanges[0] = new PushConstantRange
                {
                    Offset = 0,
                    Size = (uint)Marshal.SizeOf<Vector4>(),
                    StageFlags = ShaderStageFlags.Fragment,
                };

                PipelineLayoutCreateInfo layoutCreateInfo = new PipelineLayoutCreateInfo
                {
                    StructureType = StructureType.PipelineLayoutCreateInfo,
                    SetLayoutCount = 1,
                    SetLayouts = (IntPtr)setLayouts,
                    PushConstantRangeCount = 1,
                    PushConstantRanges = (IntPtr)pushConstantRanges,
                };
                pipelineLayout = device.CreatePipelineLayout(ref layoutCreateInfo);
            }
            
            IntPtr shaderEntryPointName = Marshal.StringToHGlobalAnsi(SHADER_ENTRY_POINT_FUNCTION_NAME);
            PipelineShaderStageCreateInfo* stageCreateInfos = stackalloc PipelineShaderStageCreateInfo[shaders.Length];
            for (int i = 0; i < shaders.Length; i++)
            {
                stageCreateInfos[i] = new PipelineShaderStageCreateInfo
                {
                    StructureType = StructureType.PipelineShaderStageCreateInfo,
                    Module = shaders[i].Module,
                    Name = shaderEntryPointName,
                    SpecializationInfo = IntPtr.Zero,
                    Stage = shaders[i].Stage,
                };
            }

            PipelineColorBlendAttachmentState colorBlendAttachmentState = new PipelineColorBlendAttachmentState
            {
                ColorWriteMask = ColorComponentFlags.R | ColorComponentFlags.G | ColorComponentFlags.B | ColorComponentFlags.A,
                BlendEnable = false,
                // No way I am filling in the rest...
            };

            PipelineColorBlendStateCreateInfo colorBlendState = new PipelineColorBlendStateCreateInfo
            {
                StructureType = StructureType.PipelineColorBlendStateCreateInfo,
                LogicOperationEnable = false,
                LogicOperation = LogicOperation.Clear, // (Just because it's 0.)
                AttachmentCount = 1,
                Attachments = new IntPtr(&colorBlendAttachmentState),
                BlendConstants = new PipelineColorBlendStateCreateInfo.BlendConstantsArray(), // (Blending is disabled anyways, so it doesn't matter.)
            };

            PipelineDepthStencilStateCreateInfo depthStencilState = new PipelineDepthStencilStateCreateInfo
            {
                StructureType = StructureType.PipelineDepthStencilStateCreateInfo,
                StencilTestEnable = false,
                DepthBoundsTestEnable = false,
                DepthTestEnable = true,
                DepthWriteEnable = true,
                DepthCompareOperation = CompareOperation.Less,
            };

            DynamicState* dynamicStates = stackalloc DynamicState[2];
            dynamicStates[0] = DynamicState.Viewport;
            dynamicStates[1] = DynamicState.Scissor;
            PipelineDynamicStateCreateInfo dynamicState = new PipelineDynamicStateCreateInfo
            {
                StructureType = StructureType.PipelineDynamicStateCreateInfo,
                DynamicStateCount = 2,
                DynamicStates = (IntPtr)dynamicStates,
            };

            // TODO: Read more on input assembly and drawing commands.
            PipelineInputAssemblyStateCreateInfo inputAssemblyState = new PipelineInputAssemblyStateCreateInfo
            {
                StructureType = StructureType.PipelineInputAssemblyStateCreateInfo,
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleList,
            };

            PipelineMultisampleStateCreateInfo multisampleState = new PipelineMultisampleStateCreateInfo
            {
                StructureType = StructureType.PipelineMultisampleStateCreateInfo,
                RasterizationSamples = SampleCountFlags.Sample1,
                SampleShadingEnable = false, // TODO: Read more on sample shading.
                MinSampleShading = 0,
                SampleMask = IntPtr.Zero, // TODO: Read more on sample mask.
                AlphaToCoverageEnable = false, // TODO: Read more on multisample coverage.
                AlphaToOneEnable = false,
            };

            PipelineRasterizationStateCreateInfo rasterizationState = new PipelineRasterizationStateCreateInfo
            {
                StructureType = StructureType.PipelineRasterizationStateCreateInfo,
                PolygonMode = PolygonMode.Fill,
                FrontFace = FrontFace.CounterClockwise,
                CullMode = cullMode,
                RasterizerDiscardEnable = false,
                LineWidth = 1,
                DepthClampEnable = false,
                DepthBiasEnable = false, // TODO: Add depth bias. The only reason it's not here now is that all I want to do for now is make a triangle appear in the window.
                DepthBiasClamp = 0,
                DepthBiasConstantFactor = 0,
                DepthBiasSlopeFactor = 0,
            };

            PipelineTessellationStateCreateInfo tesselationState = new PipelineTessellationStateCreateInfo
            {
                StructureType = StructureType.PipelineTessellationStateCreateInfo,
                PatchControlPoints = 1,
            };

            VertexInputAttributeDescription* vertexAttributeDescriptions = stackalloc VertexInputAttributeDescription[3];
            vertexAttributeDescriptions[0] = new VertexInputAttributeDescription
            {
                Binding = 0,
                Format = Format.R32G32B32SFloat,
                Location = 0,
                Offset = (uint)Marshal.OffsetOf<Vertex>("Position"),
            };
            vertexAttributeDescriptions[1] = new VertexInputAttributeDescription
            {
                Binding = 0,
                Format = Format.R32G32B32A32SFloat,
                Location = 1,
                Offset = (uint)Marshal.OffsetOf<Vertex>("Color"),
            };
            vertexAttributeDescriptions[2] = new VertexInputAttributeDescription
            {
                Binding = 0,
                Format = Format.R32G32SFloat,
                Location = 2,
                Offset = (uint)Marshal.OffsetOf<Vertex>("TextureCoordinate"),
            };

            VertexInputBindingDescription vertexBindingDescription = new VertexInputBindingDescription
            {
                Binding = 0,
                InputRate = VertexInputRate.Vertex,
                Stride = (uint)Marshal.SizeOf<Vertex>(),
            };

            PipelineVertexInputStateCreateInfo vertexInputState = new PipelineVertexInputStateCreateInfo
            {
                StructureType = StructureType.PipelineVertexInputStateCreateInfo,
                VertexAttributeDescriptionCount = 3,
                VertexAttributeDescriptions = (IntPtr)vertexAttributeDescriptions,
                VertexBindingDescriptionCount = 1,
                VertexBindingDescriptions = new IntPtr(&vertexBindingDescription),
            };

            PipelineViewportStateCreateInfo viewportState = new PipelineViewportStateCreateInfo
            {
                StructureType = StructureType.PipelineViewportStateCreateInfo,
                ViewportCount = 1,
                Viewports = IntPtr.Zero,
                ScissorCount = 1,
                Scissors = IntPtr.Zero,
            };

            GraphicsPipelineCreateInfo createInfo = new GraphicsPipelineCreateInfo
            {
                StructureType = StructureType.GraphicsPipelineCreateInfo,
                Layout = pipelineLayout,
                RenderPass = renderPass,
                Subpass = 0,
                BasePipelineHandle = Pipeline.Null,
                BasePipelineIndex = 0,

                StageCount = 2,
                Stages = (IntPtr)stageCreateInfos,
                ColorBlendState = new IntPtr(&colorBlendState),
                DepthStencilState = new IntPtr(&depthStencilState),
                DynamicState = new IntPtr(&dynamicState),
                InputAssemblyState = new IntPtr(&inputAssemblyState),
                MultisampleState = new IntPtr(&multisampleState),
                RasterizationState = new IntPtr(&rasterizationState),
                TessellationState = new IntPtr(&tesselationState),
                VertexInputState = new IntPtr(&vertexInputState),
                ViewportState = new IntPtr(&viewportState),
            };
            pipeline = device.CreateGraphicsPipelines(PipelineCache.Null, 1, &createInfo);
            Marshal.FreeHGlobal(shaderEntryPointName);
        }

        static void CreateFrameBuffers()
        {
            frameBuffers = new Framebuffer[imageViews.Length];
            for (int i = 0; i < frameBuffers.Length; i++)
            {
                ImageView* attachments = stackalloc ImageView[2];
                attachments[0] = imageViews[i];
                attachments[1] = depthImage.ImageView;

                FramebufferCreateInfo createInfo = new FramebufferCreateInfo
                {
                    StructureType = StructureType.FramebufferCreateInfo,
                    RenderPass = renderPass,
                    Width = swapchainImageExtent.Width,
                    Height = swapchainImageExtent.Height,
                    Layers = 1,
                    AttachmentCount = 2,
                    Attachments = (IntPtr)attachments,
                };
                frameBuffers[i] = device.CreateFramebuffer(ref createInfo);
            }
        }

        static void AllocateGraphicsCommandBuffers()
        {
            commandBuffers = new CommandBuffer[imageViews.Length];
            CommandBufferAllocateInfo allocateInfo = new CommandBufferAllocateInfo
            {
                StructureType = StructureType.CommandBufferAllocateInfo,
                Level = CommandBufferLevel.Primary,
                CommandBufferCount = (uint)commandBuffers.Length,
                CommandPool = graphicsCommandPool,
            };
            device.AllocateCommandBuffers(ref allocateInfo, (CommandBuffer*)Marshal.UnsafeAddrOfPinnedArrayElement(commandBuffers, 0));

            for (int i = 0; i < commandBuffers.Length; i++)
            {
                CommandBuffer commandBuffer = commandBuffers[i];

                CommandBufferBeginInfo beginInfo = new CommandBufferBeginInfo
                {
                    StructureType = StructureType.CommandBufferBeginInfo,
                    Flags = CommandBufferUsageFlags.SimultaneousUse,
                    InheritanceInfo = IntPtr.Zero,
                };
                commandBuffer.Begin(ref beginInfo);

                ClearValue* clearValues = stackalloc ClearValue[2];
                clearValues[0].Color.Float32 = new ClearColorValue.Float32Array { Value0 = ClearColor.X, Value1 = ClearColor.Y, Value2 = ClearColor.Z, Value3 = ClearColor.W };
                clearValues[1].DepthStencil.Depth = 1;
                
                RenderPassBeginInfo renderPassBeginInfo = new RenderPassBeginInfo
                {
                    StructureType = StructureType.RenderPassBeginInfo,
                    RenderPass = renderPass,
                    RenderArea = new Rect2D(new Offset2D(0, 0), swapchainImageExtent),
                    ClearValueCount = 2,
                    ClearValues = (IntPtr)clearValues,
                    Framebuffer = frameBuffers[i],
                };
                commandBuffer.BeginRenderPass(ref renderPassBeginInfo, SubpassContents.Inline);

                commandBuffer.BindPipeline(PipelineBindPoint.Graphics, pipeline);

                Viewport viewport = new Viewport { Height = swapchainImageExtent.Height, Width = swapchainImageExtent.Width, MaxDepth = 1, MinDepth = 0, X = 0, Y = 0 };
                Rect2D scissor = new Rect2D { Extent = swapchainImageExtent, Offset = new Offset2D(0, 0) };
                commandBuffer.SetViewport(0, 1, &viewport);
                commandBuffer.SetScissor(0, 1, &scissor);

                Buffer* vertexBuffers = stackalloc Buffer[1];
                vertexBuffers[0] = vertexBuffer.Buffer;
                ulong* offsets = stackalloc ulong[1];
                offsets[0] = 0;
                commandBuffer.BindVertexBuffers(0, 1, vertexBuffers, offsets);
                commandBuffer.BindIndexBuffer(indexBuffer.Buffer, 0, IndexType.UInt32);

                DescriptorSet* descriptorSets = stackalloc DescriptorSet[1];
                descriptorSets[0] = descriptorSet;
                commandBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, pipelineLayout, 0, 1, descriptorSets, 0, null);

                Vector4* tintPushConstant = stackalloc Vector4[1];
                tintPushConstant[0] = Tint;
                commandBuffer.PushConstants(pipelineLayout, ShaderStageFlags.Fragment, 0, (uint)Marshal.SizeOf<Vector4>(), (IntPtr)tintPushConstant);

                commandBuffer.DrawIndexed((uint)vertexIndices.Length, 1, 0, 0, 0);

                commandBuffer.EndRenderPass();
                commandBuffer.End();
            }
        }

        static void UpdateApplication()
        {
            mvpMatrices.ModelMatrix *= Matrix4x4.CreateRotationX(-2 * rotationSpeed * timeSinceLastFrame) * Matrix4x4.CreateRotationY(rotationSpeed * timeSinceLastFrame);
            mvpMatrices.ViewMatrix = Matrix4x4.CreateLookAt(new Vector3(0, -1, -10), Vector3.UnitZ, Vector3.UnitY);
            mvpMatrices.ProjectionMatrix = Matrix4x4.CreatePerspectiveFieldOfView(0.45f, (float)swapchainImageExtent.Width / swapchainImageExtent.Height, 0.1f, 100f);

            SetBufferData(uniformBuffer, mvpMatricesBufferData);
        }

        static void DrawFrame()
        {
            Queue graphicsQueue = deviceQueues[graphicsQueueFamilyIndex];
            Queue presentationQueue = deviceQueues[presentationQueueFamilyIndex];

            uint imageIndex = device.AcquireNextImage(swapchain, ulong.MaxValue, aquiredImageSemaphore, Fence.Null);

            fixed (Semaphore* presentationSemaphorePointer = &presentationReadySemaphore)
            fixed (Semaphore* aquireNextImageSemaphorePointer = &aquiredImageSemaphore)
            fixed (Swapchain* swapchainPointer = &swapchain)
            {
                PipelineStageFlags* pipelineStageFlags = stackalloc PipelineStageFlags[1];
                pipelineStageFlags[0] = PipelineStageFlags.ColorAttachmentOutput;

                SubmitInfo submitInfo = new SubmitInfo
                {
                    StructureType = StructureType.SubmitInfo,
                    CommandBufferCount = 1,
                    CommandBuffers = Marshal.UnsafeAddrOfPinnedArrayElement(commandBuffers, (int)imageIndex),
                    WaitSemaphoreCount = 1,
                    WaitSemaphores = (IntPtr)aquireNextImageSemaphorePointer,
                    SignalSemaphoreCount = 1,
                    SignalSemaphores = (IntPtr)presentationSemaphorePointer,
                    WaitDstStageMask = (IntPtr)pipelineStageFlags,
                };
                graphicsQueue.Submit(1, &submitInfo, Fence.Null);

                PresentInfo presentInfo = new PresentInfo
                {
                    StructureType = StructureType.PresentInfo,
                    ImageIndices = new IntPtr(&imageIndex),
                    Results = IntPtr.Zero,
                    SwapchainCount = 1,
                    Swapchains = new IntPtr(swapchainPointer),
                    WaitSemaphoreCount = 1,
                    WaitSemaphores = new IntPtr(presentationSemaphorePointer),
                };
                presentationQueue.Present(ref presentInfo);
            }
            graphicsQueue.WaitIdle();
            presentationQueue.WaitIdle();
        }

        static void RecreateSwapchain()
        {
            DestroySwapchain();
            CreateSwapchain();

            depthImage.Destroy(device);
            CreateDepthImage();

            for (int i = 0; i < frameBuffers.Length; i++)
                device.DestroyFramebuffer(frameBuffers[i]);

            CreateFrameBuffers();

            device.FreeCommandBuffers(graphicsCommandPool, (uint)commandBuffers.Length, (CommandBuffer*)Marshal.UnsafeAddrOfPinnedArrayElement(commandBuffers, 0));
            AllocateGraphicsCommandBuffers();
        }

        static void DestroySwapchain()
        {
            device.DestroySwapchain(swapchain);
            for (int i = 0; i < imageViews.Length; i++)
                device.DestroyImageView(imageViews[i]);
        }

        static void DeInitializeVulkan()
        {
            device.WaitIdle();

            vertexBuffer.Destroy(device);
            indexBuffer.Destroy(device);
            uniformBuffer.Destroy(device);

            depthImage.Destroy(device);
            crateTexture.Destroy(device);

            device.DestroySemaphore(presentationReadySemaphore);
            device.DestroySemaphore(aquiredImageSemaphore);

            device.DestroyCommandPool(graphicsCommandPool);
            device.DestroyCommandPool(transferCommandPool);

            for (int i = 0; i < frameBuffers.Length; i++)
                device.DestroyFramebuffer(frameBuffers[i]);

            device.DestroyPipeline(pipeline);
            device.DestroyPipelineLayout(pipelineLayout);
            for (int i = 0; i < shaders.Length; i++)
                shaders[i].Destroy(device);

            device.DestroyDescriptorPool(descriptorPool);
            device.DestroyDescriptorSetLayout(descriptorSetLayout);

            device.DestroyRenderPass(renderPass);
            DestroySwapchain();
            device.Destroy();
            instance.DestroySurface(surface);

            CallFunctionOnInstance<DestroyDebugReportDelegate>("vkDestroyDebugReportCallbackEXT", func => func(instance, debugReport, null), "Validation layers extension missing!");
            instance.Destroy();
        }

        static Shader LoadShader(string path, ShaderStageFlags stage)
        {
            byte[] codeArray = File.ReadAllBytes(path);
            ShaderModuleCreateInfo moduleCreateInfo = new ShaderModuleCreateInfo
            {
                StructureType = StructureType.ShaderModuleCreateInfo,
                Code = Marshal.UnsafeAddrOfPinnedArrayElement(codeArray, 0),
                CodeSize = codeArray.Length,
            };
            ShaderModule module = device.CreateShaderModule(ref moduleCreateInfo);
            return new Shader(module, stage);
        }

        static BufferResource CreateBuffer(ulong size, BufferUsageFlags usage, MemoryPropertyFlags memoryPropertyFlags)
        {
            BufferCreateInfo createInfo = new BufferCreateInfo
            {
                StructureType = StructureType.BufferCreateInfo,
                Size = size,
                Usage = usage,
                SharingMode = SharingMode.Exclusive,
                QueueFamilyIndexCount = 1,
                QueueFamilyIndices = IntPtr.Zero,
            };
            Buffer buffer = device.CreateBuffer(ref createInfo);

            device.GetBufferMemoryRequirements(buffer, out MemoryRequirements memoryRequirements);
            DeviceMemory memory = AllocateDeviceMemory(memoryRequirements, memoryPropertyFlags);

            device.BindBufferMemory(buffer, memory, 0);
            
            return new BufferResource(size, buffer, memory);
        }

        static void SetBufferData<T>(BufferResource buffer, T[] data)
            where T : struct
        {
            if ((ulong)(Marshal.SizeOf<T>() * data.Length) != buffer.Size)
                throw new ArgumentOutOfRangeException("Attempt to resize buffer data! When setting the data of a buffer, make sure the size of the data is the same is the size specified on creation.");

            IntPtr mappedMemory = device.MapMemory(buffer.Memory, 0, buffer.Size, MemoryMapFlags.None);
            System.Buffer.MemoryCopy(Marshal.UnsafeAddrOfPinnedArrayElement(data, 0).ToPointer(), mappedMemory.ToPointer(), (long)buffer.Size, (long)buffer.Size);
            device.UnmapMemory(buffer.Memory);
        }

        static void CopyBuffer(BufferResource source, BufferResource destination)
        {
            if (source.Size > destination.Size)
                throw new ArgumentOutOfRangeException("When copying a buffer, the destination's size cannot be bigger than the source's size.");

            CommandBuffer copyCommand = BeginSingleTimeCommandBuffer(transferCommandPool);
            {
                BufferCopy copyCommandRegion = new BufferCopy
                {
                    Size = source.Size,
                    SourceOffset = 0,
                    DestinationOffset = 0,
                };
                copyCommand.CopyBuffer(source.Buffer, destination.Buffer, 1, &copyCommandRegion);
            }
            EndSingleTimeCommandBuffer(copyCommand, transferCommandPool, deviceQueues[transferQueueFamilyIndex]);
        }

        static ImageResource CreateImage(Extent2D extent, Format format, ImageTiling tiling, ImageUsageFlags usage, ImageAspectFlags imageAspect, MemoryPropertyFlags memoryPropertyFlags, bool preinitialized = false)
        {
            ImageLayout initialLayout = preinitialized ? ImageLayout.Preinitialized : ImageLayout.Undefined;
            ImageCreateInfo createInfo = new ImageCreateInfo
            {
                StructureType = StructureType.ImageCreateInfo,
                Extent = new Extent3D(extent.Width, extent.Height, 1),
                Format = format,
                Tiling = tiling,
                Usage = usage,
                ImageType = ImageType.Image2D,
                SharingMode = SharingMode.Exclusive,
                QueueFamilyIndexCount = 0,
                QueueFamilyIndices = IntPtr.Zero,
                ArrayLayers = 1,
                MipLevels = 1,
                Samples = SampleCountFlags.Sample1,
                InitialLayout = initialLayout,
            };
            Image image = device.CreateImage(ref createInfo);

            device.GetImageMemoryRequirements(image, out MemoryRequirements memoryRequirements);
            DeviceMemory memory = AllocateDeviceMemory(memoryRequirements, memoryPropertyFlags);

            device.BindImageMemory(image, memory, 0);

            ImageViewCreateInfo viewCreateInfo = new ImageViewCreateInfo
            {
                StructureType = StructureType.ImageViewCreateInfo,
                Format = format,
                Image = image,
                SubresourceRange = new ImageSubresourceRange(imageAspect, 0, 1, 0, 1),
                Components = new ComponentMapping(ComponentSwizzle.Identity),
                ViewType = ImageViewType.Image2D,
            };
            ImageView view = device.CreateImageView(ref viewCreateInfo);
            return new ImageResource(image, view, memory, format, imageAspect, extent) { LastSetLayout = initialLayout };
        }
        
        static void ChangeImageLayout(ref ImageResource image, ImageLayout newLayout)
        {
            CommandBuffer command = BeginSingleTimeCommandBuffer(graphicsCommandPool);
            {
                PipelineStageFlags sourceStage;
                PipelineStageFlags destinationStage;
                AccessFlags sourceAccessMask;
                AccessFlags destinationAccessMask;

                if (image.LastSetLayout == ImageLayout.Undefined)
                {
                    sourceAccessMask = AccessFlags.None;
                    sourceStage = PipelineStageFlags.TopOfPipe;

                    if (newLayout == ImageLayout.TransferDestinationOptimal)
                    {
                        destinationAccessMask = AccessFlags.TransferWrite;
                        destinationStage = PipelineStageFlags.Transfer;
                    }
                    else if (newLayout == ImageLayout.DepthStencilAttachmentOptimal)
                    {
                        destinationAccessMask = AccessFlags.DepthStencilAttachmentRead | AccessFlags.DepthStencilAttachmentWrite;
                        destinationStage = PipelineStageFlags.EarlyFragmentTests;
                    }
                    else
                        throw new ArgumentException("Failed to change image layout. Source Layout: " + image.LastSetLayout + ", Destination Layout: " + newLayout);
                }
                else if (image.LastSetLayout == ImageLayout.TransferDestinationOptimal && newLayout == ImageLayout.ShaderReadOnlyOptimal)
                {
                    sourceAccessMask = AccessFlags.TransferWrite;
                    destinationAccessMask = AccessFlags.ShaderRead;
                    sourceStage = PipelineStageFlags.Transfer;
                    destinationStage = PipelineStageFlags.FragmentShader;
                }
                else
                    throw new ArgumentException("Failed to change image layout. Source Layout: " + image.LastSetLayout + ", Destination Layout: " + newLayout);

                ImageMemoryBarrier memoryBarrier = new ImageMemoryBarrier
                {
                    StructureType = StructureType.ImageMemoryBarrier,
                    Image = image.Image,
                    OldLayout = image.LastSetLayout,
                    NewLayout = newLayout,
                    SubresourceRange = new ImageSubresourceRange(image.Aspect, 0, 1, 0, 1),
                    SourceAccessMask = sourceAccessMask,
                    DestinationAccessMask = destinationAccessMask,
                    
                    SourceQueueFamilyIndex = Vulkan.QueueFamilyIgnored,
                    DestinationQueueFamilyIndex = Vulkan.QueueFamilyIgnored,
                };
                command.PipelineBarrier(sourceStage, destinationStage, DependencyFlags.None, 0, null, 0, null, 1, &memoryBarrier);
            }
            EndSingleTimeCommandBuffer(command, graphicsCommandPool, deviceQueues[graphicsQueueFamilyIndex]);
            image.LastSetLayout = newLayout;
        }

        static void CopyBufferToImage(BufferResource buffer, ImageResource image)
        {
            CommandBuffer command = BeginSingleTimeCommandBuffer(transferCommandPool);
            {
                BufferImageCopy copyRegion = new BufferImageCopy
                {
                    BufferImageHeight = image.Extent.Height,
                    BufferRowLength = image.Extent.Width,
                    BufferOffset = 0,
                    ImageOffset = new Offset3D(0, 0, 0),
                    ImageSubresource = new ImageSubresourceLayers(image.Aspect, 0, 1, 0),
                    ImageExtent = new Extent3D(image.Extent.Width, image.Extent.Height, 1),
                };
                command.CopyBufferToImage(buffer.Buffer, image.Image, image.LastSetLayout, 1, &copyRegion);
            }
            EndSingleTimeCommandBuffer(command, transferCommandPool, deviceQueues[transferQueueFamilyIndex]);
        }

        static Texture LoadTexture(string path, uint binding)
        {
            ImageResource image;
            Bitmap bitmap = new Bitmap(path);
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            {
                ulong imageSize = (ulong)(bitmap.Width * bitmap.Height * 4);
                BufferResource stagingBuffer = CreateBuffer(imageSize, BufferUsageFlags.TransferSource, MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);

                IntPtr stagingBufferDataLocation = device.MapMemory(stagingBuffer.Memory, 0, stagingBuffer.Size, MemoryMapFlags.None);
                System.Buffer.MemoryCopy(bitmapData.Scan0.ToPointer(), stagingBufferDataLocation.ToPointer(), stagingBuffer.Size, stagingBuffer.Size);
                device.UnmapMemory(stagingBuffer.Memory);

                image = CreateImage(new Extent2D((uint)bitmap.Width, (uint)bitmap.Height), Format.R8G8B8A8UNorm, ImageTiling.Optimal, ImageUsageFlags.Sampled | ImageUsageFlags.TransferDestination, ImageAspectFlags.Color, MemoryPropertyFlags.DeviceLocal);
                ChangeImageLayout(ref image, ImageLayout.TransferDestinationOptimal);
                CopyBufferToImage(stagingBuffer, image);
                ChangeImageLayout(ref image, ImageLayout.ShaderReadOnlyOptimal);
                
                stagingBuffer.Destroy(device);
            }
            bitmap.UnlockBits(bitmapData);
            bitmap.Dispose();

            SamplerCreateInfo samplerCreateInfo = new SamplerCreateInfo
            {
                StructureType = StructureType.SamplerCreateInfo,
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                AddressModeU = SamplerAddressMode.ClampToBorder,
                AddressModeV = SamplerAddressMode.ClampToBorder,
                AddressModeW = SamplerAddressMode.ClampToBorder,
                AnisotropyEnable = true,
                MaxAnisotropy = 16,
                BorderColor = BorderColor.IntOpaqueBlack,
                UnnormalizedCoordinates = false,
                CompareEnable = false,
                CompareOperation = CompareOperation.Always,
                MipmapMode = SamplerMipmapMode.Linear,
                MipLodBias = 0,
                MinLod = 0,
                MaxLod = 0,
            };
            Sampler sampler = device.CreateSampler(ref samplerCreateInfo);
            return new Texture(image, sampler);
        }

        static DeviceMemory AllocateDeviceMemory(MemoryRequirements requirements, MemoryPropertyFlags propertyFlags)
        {
            uint memoryTypeIndex = 0;
            physicalDevice.GetMemoryProperties(out PhysicalDeviceMemoryProperties memoryProperties);

            MemoryType* memoryType = &memoryProperties.MemoryTypes.Value0;
            for (uint i = 0; i < memoryProperties.MemoryTypeCount; i++)
            {
                if (memoryType->PropertyFlags.HasFlag(propertyFlags)
                    && (((1 << (int)i) & requirements.MemoryTypeBits) != 0))
                {
                    memoryTypeIndex = i;
                    break;
                }
                memoryType += 1;
            }

            MemoryAllocateInfo allocationInfo = new MemoryAllocateInfo
            {
                StructureType = StructureType.MemoryAllocateInfo,
                AllocationSize = requirements.Size,
                MemoryTypeIndex = memoryTypeIndex,
            };
            return device.AllocateMemory(ref allocationInfo);
        }

        static CommandBuffer BeginSingleTimeCommandBuffer(CommandPool pool)
        {
            CommandBuffer command;
            CommandBufferAllocateInfo allocationInfo = new CommandBufferAllocateInfo
            {
                StructureType = StructureType.CommandBufferAllocateInfo,
                Level = CommandBufferLevel.Primary,
                CommandPool = pool,
                CommandBufferCount = 1,
            };
            device.AllocateCommandBuffers(ref allocationInfo, &command);
            
            CommandBufferBeginInfo beginInfo = new CommandBufferBeginInfo
            {
                StructureType = StructureType.CommandBufferBeginInfo,
                Flags = CommandBufferUsageFlags.OneTimeSubmit,
                InheritanceInfo = IntPtr.Zero,
            };
            command.Begin(ref beginInfo);
            return command;
        }

        static void EndSingleTimeCommandBuffer(CommandBuffer command, CommandPool pool, Queue queue)
        {
            command.End();

            SubmitInfo copyCommandSubmitInfo = new SubmitInfo
            {
                StructureType = StructureType.SubmitInfo,
                CommandBufferCount = 1,
                CommandBuffers = new IntPtr(&command),
                WaitSemaphoreCount = 0,
                WaitSemaphores = IntPtr.Zero,
                SignalSemaphoreCount = 0,
                SignalSemaphores = IntPtr.Zero,
                WaitDstStageMask = IntPtr.Zero,
            };
            queue.Submit(1, &copyCommandSubmitInfo, Fence.Null);
            queue.WaitIdle();

            device.FreeCommandBuffers(pool, 1, &command);
        }

        static void CallFunctionOnInstance<TFunctionDel>(string functionName, Action<TFunctionDel> call, string failMessage = null)
        {
            if (failMessage == null)
                failMessage = "Faild to find function " + functionName + ".";

            IntPtr functionNamePtr = Marshal.StringToHGlobalAnsi(functionName);
            IntPtr functionPointer = instance.GetProcAddress((byte*)functionNamePtr);
            if (functionPointer == IntPtr.Zero)
                throw new Exception(failMessage);
            else
            {
                TFunctionDel function = Marshal.GetDelegateForFunctionPointer<TFunctionDel>(functionPointer);
                call(function);
            }
            Marshal.FreeHGlobal(functionNamePtr);
        }
    }
}

*/