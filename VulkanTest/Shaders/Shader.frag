#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 vertexColor;
layout(location = 1) in vec2 vertexTextureCoordinate;

layout(location = 0) out vec4 fragmentColor;
layout(binding = 1) uniform sampler2D textureSampler;

layout(push_constant) uniform PushConstants
{
	vec4 tint;
} pushConstants;

void main()
{
	fragmentColor = vec4(vertexColor, 1) * pushConstants.tint * texture(textureSampler, vertexTextureCoordinate);
}